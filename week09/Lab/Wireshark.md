## WireShark NAT Lab
>This lab was about focused on exploring how Network Address Translation (NAT) worked by analyzing a trace file from the client side and one from the server side. After going through the lab I am thoroughly convinced in NATs ability to mask/translate private to public ip address.  
### Process
To complete this lab, I followed the instructions listed on the Wireshark Lab version 8.1 for [NAT](https://www-net.cs.umass.edu/wireshark-labs/Wireshark_NAT_v8.0.pdf) 
### Collab
NONE
### Questions
### 1. What is the IP address of the client?
192.168.1.100
#### 2. The client actually communicates with several different Google servers in order to implement “safe browsing.” (See extra credit section at the end of this lab). The main Google server that will serve up the main Google web page has IP address 64.233.169.104. In order to display only those frames containing HTTP messages that are sent to/from this Google, server, enter the expression “http && ip.addr == 64.233.169.104” (without quotes) into the Filter: field in Wireshark.
Complete
#### 3. Consider now the HTTP GET sent from the client to the Google server (whose IP address is IP address 64.233.169.104) at time 7.109267. What are the source and destination IP addresses and TCP source and destination ports on the IP datagram carrying this HTTP GET?
`Source Address: `192.168.1.100 /4335

`Destination Address: `64.233.169.104 /80
#### 4. At what time is the corresponding 200 OK HTTP message received from the Google server? What are the source and destination IP addresses and TCP source and destination ports on the IP datagram carrying this HTTP 200 OK message?
`Time:` 07.427932

`Source:` 64.233.169.104/80

`Destination:` 192.168.1.100/4335 
#### 5. Recall that before a GET command can be sent to an HTTP server, TCP must first set up a connection using the three-way SYN/ACK handshake. At what time is the client-to-server TCP SYN segment sent that sets up the connection used by the GET sent at time 7.109267? What are the source and destination IP addresses and source and destination ports for the TCP SYN segment? What are the source and destination IP addresses and source and destination ports of the ACK sent in response to the SYN. At what time is this ACK received at the client? (Note: to find these segments you will need to clear the Filter expression you entered above in step 2. If you enter the filter “tcp”, only TCP segments will be displayed by Wireshark).
>**SYN**
* `Time:` 07.344792
* `Source(TCP SYN):` 192.168.1.100 /4335
* `Destination(TCP SYN): `64.233.169.104 /80

>**ACK**
* `Source:` 64.233.169.104/80
* `Destination:` 192.168.1.100/4335
* `Time:` 07.378121
#### 6. In the NAT_ISP_side trace file, find the HTTP GET message was sent from the client to the Google server at time 7.109267 (where t=7.109267 is time at which this was sent as recorded in the NAT_home_side trace file). At what time does this message appear in the NAT_ISP_side trace file? What are the source and destination IP addresses and TCP source and destination ports on the IP datagram carrying this HTTP GET (as recording in the NAT_ISP_side trace file)? Which of these fields are the same, and which are different, than in your answer to question 3 above?
`Time:` 07.800232

`Source:` 71.192.34.104/4335

`Destination: ` 64.233.169.104/80
>The source IP address has changed from question 3 above.
#### 7. Are any fields in the HTTP GET message changed? Which of the following fields in the IP datagram carrying the HTTP GET are changed: Version, Header Length, Flags, Checksum. If any of these fields have changed, give a reason (in one sentence) stating why this field needed to change.
>`Checksum` is the only field that I observed having a change between ISP and Home side. I believe its mainly due to the fact that the two files are not in sync. 
#### 8. In the NAT_ISP_side trace file, at what time is the first 200 OK HTTP message received from the Google server? What are the source and destination IP addresses and TCP source and destination ports on the IP datagram carrying this HTTP 200 OK message? Which of these fields are the same, and which are different than your answer to question 4 above?
`Time: ` 07.848634

`Source:` 64.233.169.104/80

`Destination:` 71.192.34.104/4335
>The destination IP address of this answer is to the router before hitting the NAT table.
#### 9. In the NAT_ISP_side trace file, at what time were the client-to-server TCP SYN segment and the server-to-client TCP ACK segment corresponding to the segments in question 5 above captured? What are the source and destination IP addresses and source and destination ports for these two segments? Which of these fields are the same, and which are different than your answer to question 5 above?
***TCP SYN***
`Time:` 07.766539
`Source:` 71.192.34.104/4335
`Destination:` 64.233.169.104/80

***TCP ACK***

`Time:` 07.798839
`Source:` 64.233.169.104/80
`Destination:` 71.192.34.104/4335

>Once again, the difference between the client and isp side is the masquerading of the host ip address between a NAT table. 
#### 10. Using your answers to 1-8 above, fill in the NAT translation table entries for HTTP connection considered in questions 1-8 above.
| External | Internal | Host |
| ---------------- | ----------- | ----| 
| 71.192.34.104, 4335 | 192.168.1.100, 4335 |  1  | 
| 24.34.112.235, 5002 | 192.168.1.1, 3445 |  1  |
| 24.34.112.235, 6772  | 192.168.1.2, 4445 |  2  |
| 24.34.112.235, 6457  | 192.168.1.2, 4554 |  2  |
| 24.34.112.235, 5242  | 192.168.1.3, 2546 |  3  |
| 24.34.112.235, 5899  | 192.168.1.3, 2648 |  3  |