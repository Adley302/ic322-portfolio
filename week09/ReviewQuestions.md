#### R5. What is the “count to infinity” problem in distance vector routing?
>The route to infinity problem is the result of a change in link cost. The change in the link cost or availability of the link can take a long time to discover, because other routers in the chain are still advertising that they can reach said link based on old information. During this time, as the routers update each other, the cost of getting to the damaged link will increment by one as it tries to compute the min of its old path and the new one it is receiving. 

#### R8. True or false: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.
> `False:` Open Shortest Path First (OSPF), broadcasts routing information to all other routers in the autonomous system, not just to its neighboring routers. Broadcasts are sent out by the router whenever there is a change in a link state as well as periodically every 30 minutes regardless of change. 

#### R9. What is meant by an area in an OSPF autonomous system? Why was the concept of an area introduced?
> An area in an OSPF autonomous system is a grouping of routers within the same Intra-AS. Essentially, it is another method to compartmentalize subgroups into smaller families. With a backbone area configured, it makes more efficient to distribute the information along all parts of the Intra-AS itself. 

#### P3. Consider the following network. With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1.
![Alt Text](/week09/Images/P3.JPG) 

| Step | N'     | D(y),P(y) | D(z),P(z) | D(t),P(t) | D(v),P(v) | D(w),P(w) | D(u),P(u) |
| ---- | --     | --------- | --------- | --------- | --------- | --------- | --------- |
|  0   | x      |   6,x     |   8,x     |     ∞     |   `3,x`   |     6,x   |     ∞     |
|  1   | xv     |   6,x     |   8,x     |    7,v    |    ∞      |   `6,x`   |    6,v    |
|  2   | xvw    |   `6,x`   |   8,x     |    7,v    |  &check;  |   &check; |    6,v    |
|  3   | xvwy   |   &check; |   8,x     |    7,v    |  &check;  |   &check; |   `6,v`   |
|  4   | xvwyu  |   &check; |   8,x     |  `7,v`    |  &check;  |   &check; |   &check; |
|  5   | xvwyut |  &check;  |   `8,x`   |  &check;  |  &check;  |   &check; |   &check; |
|  6   | xvwyutz|  &check;  |  &check;  |  &check;  |  &check;  |   &check; |   &check; |

#### P4. Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following:(only nodes v, y, and w).
* Compute the shortest path from v to all network nodes.

| Step | N'     | D(y),P(y) | D(z),P(z) | D(t),P(t) | D(x),P(x) | D(w),P(w) | D(u),P(u) |
| ---- | --     | --------- | --------- | --------- | --------- | --------- | --------- |
|  0   | v      |   8,v     |   ∞       |    4,v    |   `3,v`   |     4,v   |    3,v    |
|  1   | vx     |   8,v     |   11,x    |    4,v    |  &check;  |    4,v    |    `3,v`  |
|  2   | vxu    |   8,v     |   11,x    |  `4,v`    |  &check;  |   4,v     |   &check; |
|  3   | vxut   |   8,v     |   11,x    |  &check;  |  &check;  |   `4,v`   |   &check; |
|  4   | vxutw  |  `8,v`    |   11,x    |  &check;  |  &check;  |   &check; |   &check; |
|  5   | xvwyut |  &check;  |   `11,x`  |  &check;  |  &check;  |   &check; |   &check; |
|  6   | xvwyutz|  &check;  |  &check;  |  &check;  |  &check;  |   &check; |   &check; |
* Compute the shortest path from w to all network nodes.

| Step | N'     | D(y),P(y) | D(z),P(z) | D(t),P(t) | D(v),P(v) | D(x),P(x) | D(u),P(u) |
| ---- | --     | --------- | --------- | --------- | --------- | --------- | --------- |
|  0   | w      |   ∞       |   ∞       |     ∞     |   4,w     |     6,w   |   `3,w`   |
|  1   | wu     |   ∞       |   ∞       |    5,u    |  `4,w`    |    6,w    |   &check; |
|  2   | wuv    |   12,v    |   ∞       |  `5,u`    |  &check;  |   6,w     |   &check; |
|  3   | wuvt   |   12,v    |   8,x     |  &check;  |  &check;  |   `6,w`   |   &check; |
|  4   | wuvtx  |   12,v    |   `8,x`   |  &check;  |  &check;  |   &check; |   &check; |
|  5   | wuvtxz |   `12,v`  |  &check;  |  &check;  |  &check;  |   &check; |   &check; |
|  6   | wuvtxzy|  &check;  |  &check;  |  &check;  |  &check;  |   &check; |   &check; |
* Compute the shortest path from y to all network nodes.

| Step | N'     | D(x),P(x) | D(z),P(z) | D(t),P(t) | D(v),P(v) | D(w),P(w) | D(u),P(u) |
| ---- | --     | --------- | --------- | --------- | --------- | --------- | --------- |
|  0   | y      |   `6,y`   |   12,y    |    7,y    |   8,y     |     ∞     |     ∞     |
|  1   | yx     |  &check;  |   12,y    |   `7,y`   |    8,y    |    12,x   |    ∞      |
|  2   | yxt    |  &check;  |   12,y    |  &check;  |  `8,y`    |    12,x   |    9,t    |
|  3   | yxtv   |   &check; |   12,y    |  &check;  |  &check;  |    12,x   |   `9,t`   |
|  4   | yxtvu  |   &check; |  `12,y`   |  &check;  |  &check;  |    12,x   |   &check; |
|  5   | xvwyut |  &check;  |  &check;  |  &check;  |  &check;  |   `12,x`  |   &check; |
|  6   | xvwyutz|  &check;  |  &check;  |  &check;  |  &check;  |   &check; |   &check; |

#### P5. Consider the network shown below, and assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node z.
![Alt Text](/week09/Images/P5.JPG)

|Round One | u   | v | y | x | z |
|-|-    |-  |-  |-  |-  |
|v|  ∞  | ∞ | ∞ | ∞ | ∞ |
|x|  ∞  | ∞ | ∞ | ∞ | ∞ | 
|z|  ∞  | 6 | ∞ | 2 | 0 |

|Round Two  | u   | v | y | x | z |
|-          |-    |-  |-  |-  |-  |
|v          |  1  | 0 | ∞ | 3 | 5 |
|x          |  ∞  | 3 | 3 | 0 | 2 | 
|z          |  7  | 5 | 5 | 2 | 0 |

|Round Three| u   | v | y | x | z |
|-          |-    |-  |-  |-  |-  |
|v          |  1  | 0 | 3 | 3 | 5 |
|x          |  4  | 3 | 3 | 0 | 2 | 
|z          |  7  | 5 | 5 | 2 | 0 |

#### P11. Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: c(x,y) = 4, c(x,z) = 50, c(y,w) = 1, c(z,w) = 1, c(y,z) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm.
![Alt Text](/week09/Images/Figure_5.7.JPG)

* a. When the distance vector routing is stabilized, router w, y, and z inform their distances to x to each other. What distance values do they tell each other?
    >    `Dy(X) = 4`
    
    > `Dz(X) = 7`
    
    > `Dw(X) = 5`

* b. Now suppose that the link cost between x and y increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer.
    >Because we are dealing with more than just two neighboring nodes in this case, the count-to-infinity problem will still persist. It would take 44 interations again to reach a stable state. Assuming that poisoned reverse is used between `y` and `z`, the algorithm would still have the distance that `w` is tracking to choose from which will iterate from 6 all the way to 50 (50-6 = 44).
* c. How do you modify c(y,z) such that there is no count-to-infinity problem 
at all if c(y,x) changes from 4 to 60?
    > in order to make it so that there is no count-to-infinity problem, you would have to update c(y,z) to be a greater cost than c(y,x). In doing this then `y` would have would only need to worry about the loop with `w`. In the case that there is only two node loop consideration, poisoned reverse can be used to fix the count-to-infinity problem. 