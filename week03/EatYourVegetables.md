## R16: suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice’s host to Bob’s host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.
Alice will write up the message from her host machine. The machine will then forward the message to her mail server via SMTP or HTTP. From Alices mail server, the server will then send the message over to Bobs mail server via SMTP. Once the message has arrived at Bobs mail server, it will be forwarded to bobs agent on his host machine using HTTP or IMAP. 

## R18:  What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?
HOL is Head of Line Blocking. It is the concept that if a user is requesting a web page consisting of multiple small 
objects and one large object. If the large object is at the beginning, then all of the smaller objects must wait for 
large object to be sent in order for them to be sent. HTTP/2 attempts to fix this by allowing portions of the smaller 
objects to be sent in the frames of the larger object.

## R24:  CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them.
Enter Deep - The philosophy of getting close to the end user by deploying server clusters in access ISPs all over 
the world. This will help to improve user-perceived delay and throuput by reducing the number of links in the network
core that the content must traverse. 

## P18: a. What is a whois database? b. Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used.
A whois database is a database that houses the registration information whenever someone registers a domain name or 
updates their DNS settings. 
Whois database: who.is
domain:         youtube.com
dns server:     ns1.google.com -- 216.239.32.10
dns server2:    ns2.google.com -- 216.239.34.10

Whois database: whois.com
domain:         hulu.com
dns server:     ns1-24.akam.net 
dns server2:    use9.akam.net 

## P20:  Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the Web servers (outside your department) that are most popular among the users in your department? Explain.
1) method one would be looking at the saved addresses on the local cache of the department itself. You can review which ones have a more updated last modified. Conversely, you could also keep track of which domains come back the quickest after the their TTL has expired.  

## P21:  Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain.
If you have access to the cache records, you could look at the when it was recently added via the resource record. if not, you could compare the query time of the website.  