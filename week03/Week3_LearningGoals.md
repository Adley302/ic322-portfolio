# I can explain how a the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.
- To begin, DNS commonly runs over UDP using port 53. It is employed by other application layer protocols to translate user-supplied hostnames to IP addresses. The DNS application is run on the user machine. 
  There are 3 classes of DNS servers
  -- Root DNS Server
  -- Top-Level Domain (TLD)
  -- Authoritative Server

  Not included in this list is the client side DNS server that simply runs on your user machine as mentioned above. If a user is typing a website address in a browser URL, the following is what DNS does behind the scenes in order to locate where the website the user is looking for is located.
  (quick thing to note, if the hostname IP address is already noted (cached) on your local DNS server, then there is no need for the additional traffic lines)

  1. The client DNS makes contact with the local DNS server
  2. The local DNS server then contacts one of the root servers
  3. The root server returns the IP Address of the TLD server for that
     particular domain (.com, .edu, .org, etc..)
  4. The client then contacts one of the TLD servers who returns the IP
     address of an authoritative server for the requested hostname
  5. The client then contacts one of the authoritative servers for the
     hostname which returns an IP address for said URL

# I can explain the role of each DNS record type.
#### A resource record a quadtuple consisted of the following fields:
- (Name, Value, Type, TTL)

#### Type: the meaning of Name and Value depend on the type
- Type A: 
        Name is a hostname, standard hostname-to-ip address mapping
        Value if the IP address.
- Type NS: 
        Name is a domain (such as foo.com)
        Value is the hostname of an authoritative DNS server that knows how to obtain
        the IP addresses for hosts in the domain. ie (foo.com, dns.foo.com)
- Type CNAME:
        Value is the canonical name of a mail server that has an alias hostname 'Name'
        this record can provide queries the canonical name for a host. ie. (foo.com, 
        relay1.bar.foo.com, CNAME)
- Type MX:
        Value is the canonical name of a mail server that has an alias hostname Name. ie. 
        (foo.com, mail.bar.foo.com, MX). MX records let hostnames of mail servers to have simple
        aliases. By using an MX record, a company can have the same aliased name for its mail server 
        and for one of its other servers (such as web server).

#### TTL (time to live)
    The time to live simply defines how the information will be cached for. 

# I can explain the role of the SMTP, IMAP, and POP protocols in the email system.
### The internet mail system is made up of 3 major components
- user agents (GMAIL, Outlook)
- mail servers
- SMTP

### Simple Mail Transfer Protocol (SMTP, port 25)
    SMTP is the principle application layer protocol for Internet electronic mail. It 
    uses reliable data across TCP to transfer mail from the senders mail server to the 
    recipients mail server. Its important that SMTP does not use any intermediate mail 
    servers, so the message isn't lost somewhere if a connection fails. 
### Internet Mail Access Protocol (IMAP)
    IMAP allows users to manage folders and contents contained in their email servers. an
    example of this is the different settings and notification you can make when using outlook,
    those preferences are made using IMAP. *IMPORTANT* the actual carrying of the email contents
    is still processed through SMTP, IMAP comes into play once it has arrived on the recipient email 
    server

# I know what each of the following tools are used for: nslookup, dig, whois.

    nslookup (Name Server Lookup):
    - tool is used to send a DNS query to any DNS server. typically used to obtain a domain name/mapping
    via the command line of your desired hostname or ip address. 

    dig (domain information groper):
    - dig provides the same information as nslookup 
    - the main difference is that, unlike nslookup, dig utilizes the operating system library to
      give a more consistent and detailed result. It does not include the ipv6 addresses that nslookup does
      but it include the query time, server, time of loopup, as well as additional header information
    
    whois:
    - this command searches a user-name database to retrieve back information regarding a hostname.
    - the system contains a listing of records that has details about both the domain and the owners.  

    
