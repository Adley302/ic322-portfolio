## I can explain how TCP and UDP multiplex messages between processes using sockets.
In a broad view of the concept, multiplexing is a component of the transport layer that
is used to extend the host-to-host delivery service provided by the network layer to 
a process-to-process delivery service for applications running on the hosts. 

The job of delivering data in a transport layer segment to the correct socket is called demultiplexing.
The job of gathering data chunks at the source from different sockets, encapsulating each data chunk with
header information to create segments, and passing the segments to the network layer is known as
multiplexing. 

sockets are simply doors which data passes to the network. multiplexing is the act of combining the data
from a process, labeling it with header information and giving it to the mail man (network layer) to deliver.
meanwhile, demultiplexing is the act of receiving the data from the mail man and breaking each letter down
to whom in the household each one must go to. 

## I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.
TCP:
- services: reliable data transfer, congestion control, error checking, process-to-process data delivery.
offers reliable data transfer... using flow control, sequence numbers, acknowledgments, and timers, TCP ensures that data is delivered from the sending process to the receiving process correctly and in order.

UDP:
- performs about as little as a transport protocol can do, its primary services are process-to-process data delivery and error checking
UDP takes messages from the application process, attaches source and destination port number fields for the multiplexing/
demultiplexing service, adds two other small fields, and passes the resulting segment to the network layer. UDP makes a best
effort attempt to deliever the segments, nothing is guaranteed nor verified. 

The upside of choosing UDP is mainly the added benefit of not dealing with any network congestion and sending out the packets as soon as possible. Due to there being no delay to establish a connection, the information is sent out immediately. Additionally, because of the no connection state, a server can support more active udp clients when the application runs over UDP as apposed to TCP. One more cherry on top is the fact that when it comes to header information, UDP contains 8 bytes of overhead compared to the 20 bytesthat TCP packs on. 

## I can explain how and why the following mechanisms are used and which are used in TCP): sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.
sequence numbers are used to keep track of the packet that is sent. similar to a checksum/number/identifiable information of sorts. A sequence number is "hey, I am this packet", so that sender and receiver are both tracking which packet is which. 

duplicate ACKs tells the sender that the receiver did not correctly receive the last packet that was sent. It the receiver will instead send the acknowledgement of the last received packet. If on the sender side, that ACK comes twice in a row, then it knows that the previously sent packet on the sender side was not properly transmitted to the receiver.

timers are used to help solve the problems of how to detect packet loss and what to do when packet loss occurs. Thus timer is introduced on the sender side where a time value is generated based on when packet loss is most likely, and if the ACK has not been received in that timeframe... the sender will retransmit the packet. 

pipelining is process of being able to send multiple packets without having to wait for the ACK of the previous packet. it comes with the consideration that there must be an increase in the number of sequence numbers, receiver and sender may have to buffer, and the range of sequence numbers will depend on the manner in which the exisiting protocol responds to packet loss and delay. The solutions to these concerns are identified in the varied approach of Go-back-N & selective repeat

Go-Back-N protocol allows for pipelined packet transfer without waiting for ACK, given the constaint that there is a maximum number of allowable unacknowledged packets in the pipe. with this protocol the timer restarts each time a packet is sent that is acknowledged back (given that there are more unacknowledged packets). If the timer expires, all unacknowledged packets are transmitteed to the receiver. Given that to be the case, the receiver has no need to buffer any packets and instead can drop any packets that it receives out of order, because it will get sent back all unacknowledged packets its receiving. 

Selective repeat allows for the receiver to buffer out of order packets until the correct number packet arrives. This protocol is still restrained to a maximum allowed number of unacknowledged packets (as is necessary for flow control). With selective repeat however, the concerns for mass delay when there is a large file being sent is avoided with the option to now delay and only need to retransmit specific packets from the sender to the receiver.

## I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.
As explained the in question above. When the mechanisms are combined, they can be used to approproately label packets as lost, out-of-order, or duplicate and react accordingly based on the current protocol. 