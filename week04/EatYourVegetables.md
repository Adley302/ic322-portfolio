# R5. Why is it that voice and video traffic is often sent over TCP rather than UDP in today’s Internet? (Hint: The answer we are looking for has nothing to do with TCP’s congestion-control mechanism.)
Without considering TCPs congestion control mechanism, the reason I would imagine 
that voice and video traffic are sent over TCP in today's internet is because of the
security that TCP provides. TCP comes equipped with better TLS/ encrypting of information
ability when compared to UDP. 

# R7.  Suppose a process in Host C has a UDP socket with port number 6789. Suppose both Host A and Host B each send a UDP segment to Host C with destination port number 6789. Will both of these segments be directed to the same socket at Host C? If so, how will the process at Host C know that these two segments originated from two different hosts?
Yes, both of the segments will be directed to the same socket at host C. The process at host C will be able to differentiate that the two segments originated from different hosts when it looks at the recvfrom field and the 
difference in source ports numbers. 

# R8.  Suppose that a Web server runs in Host C on port 80. Suppose this Web server uses persistent connections, and is currently receiving requests from two different Hosts, A and B. Are all of the requests being sent through the same socket at Host C? If they are being passed through different sockets, do both of the sockets have port 80? Discuss and explain.
After performing the initial handshake with the welcoming socket, the different hosts will come in through seperate sockets. When they come in through different sockets, they can still all have the same destination port, TCP will have spawned a new process for each of the clients. 

# R9.  In our rdt protocols, why did we need to introduce sequence numbers?
When dealing with reliable data transfer, sequence numbers are needed in order to keep track of the packets. They help tell us if the incoming packets is a duplicate or out of order.

# R10.  In our rdt protocols, why did we need to introduce timers?
When dealing with reliable data transfer, timers are used in order to confirm if the receiver has accurately received the packets that the sender has sent out. 

# R11. Suppose that the roundtrip delay between sender and receiver is constant and known to the sender. Would a timer still be necessary in protocol rdt 3.0, assuming that packets can be lost? Explain
A timer would be still be necessary for a protocol in rdt 3.0 in order to properly detect that a packet has been lost.

# R15. Suppose Host A sends two TCP segments back to back to Host B over a TCP connection. The first segment has sequence number 90; the second has sequence number 110.
## a. How much data is in the first segment?
* 20 bytes
## b. Suppose that the first segment is lost but the second segment arrives at B. In the acknowledgment that Host B sends to Host A, what will be the acknowledgment number?
* The acknowledgment number will be 90 to relay that a segment has not been received to the sender. 

# P3. UDP and TCP use 1s complement for their checksums. Suppose you have the following three 8-bit bytes: 01010011, 01100110, 01110100. What is the 1s complement of the sum of these 8-bit bytes? (Note that although UDP and TCP use 16-bit words in computing the checksum, for this problem you are being asked to consider 8-bit sums.) Show all work. Why is it that UDP takes the 1s complement of the sum; that is, why not just use the sum? With the 1s complement scheme, how does the receiver detect errors? Is it possible that a 1-bit error will go undetected? How about a 2-bit error?
* The 1s complement will be: 01010011 + 01100110 + 01110100 = 10101101 --> 01010010
* UDP takes the 1s complement because it's a simple way to check for bit errors. Using 1s addition, if the data was not corrupted then it should result in all 1s. 
* it is still possible for 1-bit errors to go undetected if the numbers just happen to flip the same and give a false positive. This is not the same case for a 2 bit error. 