### What is the IP address and TCP port number used by the client computer (source) that is transferring the file to gaia.cs.umass.edu?
* IP Address: 192.168.1.102, Port: 1161
### What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?
* IP Address: 128.119.245.12, Port: 80
### What is the IP address and TCP port number used by your client computer (source) to transfer the file to gaia.cs.umass.edu?
* Used provided capture file
### What is the sequence number of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? What is it in the segment that identifies the segment as a SYN segment?  
* Sequence number: 0, the sequence number of 0 identifies it as a SYN segment
### What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is the value of the Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu determine that value? What is it in the segment that identifies the segment as a SYNACK segment? 
* Seq = 0, Ack = 1, value is the addition of 1 to the seq number. 
### What is the sequence number of the TCP segment containing the HTTP POST command? 
- Seq = 1
### Consider the TCP segment containing the HTTP POST as the first segment in theTCP connection. What are the sequence numbers of the first six segments in the TCP connection (including the segment containing the HTTP POST)? At what time was each segment sent? When was the ACK for each segment received? Given the difference between when each TCP segment was sent, and when its acknowledgement was received, what is the RTT value for each of the six segments? What is the EstimatedRTT value (see Section 3.5.3, page 242 in text) after the receipt of each ACK?
* Segment 1: seq = 1, time = .026477, AckRcv = .053937, RTT = 190ms
* Segment 2: seq = 566, time = .041737, AckRcv = .077294, RTT = 190ms
* Segment 3: seq = 2026, time = .054026, AckRcv = .124085, RTT = 190ms
* Segment 4: seq = 3486, time = .054690, AckRcv = .169118, RTT = 190ms
* Segment 5: seq = 4946, time = .077405, AckRcv = .217299, RTT = 190ms
* Segment 6: seq = 6406, time = .078157, AckRcv = .267802, RTT = 190ms
### What is the length of each of the first six TCP segments?
* Segment 1 had a length of 565, segments 2-6 all have a length of 1460
### What is the minimum amount of available buffer space advertised at the receivedfor the entire trace? Does the lack of receiver buffer space ever throttle the sender?
* Minimum amount of available buffer space advertised is 6780
### Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?
* There are none within the first few segment with the same sequence and ack number as previous segment.
### How much data does the receiver typically acknowledge in an ACK? Can you identify cases where the receiver is ACKing every other received segment (see Table 3.2 on page 250 in the text)
* The receiver acknowledges the sum of the data it has been received thus far. The cases where it would Ack every other received segment would be if selective acknowledgement option was selected. 