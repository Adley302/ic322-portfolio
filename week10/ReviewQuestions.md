#### R11. How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?
> Border Gateway Protocol (BGP) is the inter-AS routing protocol used by all autonomous systems to route packets outside of their intra-AS networks. BGP is the means by which each subnet knows that other subnets exists. A subnet will broadcast that it exists and BGP makes sure that all the routers in the internet knows about that subnet. The NEXT-HOP attribute is the IP address of the router interface that begins the AS-PATH. aka, NEXT-HOP defines the entry location of the next AS along the path to the destination. The AS-PATH is the series of AS locations you have to go through in order to reach the destination. The example would be that AS-PATH gives you the locations you have to visit, while NEXT-HOP tells you how to get there.

`AS-PATH: ` Take 50 to 95 to I-4. 

`NEXT-HOP: ` Rowe Blvd

#### R13. True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.
`False:`
>After the router receives an advertised path, it will (based on its routing policies) add its own identity to the path in order to let its neighbors know that you must go through this path in order to get to the advertised final destination. 
* AS3 sends a BGP message to AS2, saying that x exists and is in AS3; let’s denote 
this message as “AS3 x”. Then AS2 sends a BGP message to AS1, saying that x 
exists and that you can get to x by first passing through AS2 and then going to AS3; 
let’s denote that message as “AS2 AS3 x”. In this manner, each of the autonomous 
systems will not only learn about the existence of x, but also learn about a path of 
autonomous systems that leads to x.

#### R19. Names four different types of ICMP messages (also describe what they are used for)
`ICMP` messages have a type and a code field, and contain the header and the first 8 bytes of the IP datagram that caused the ICMP message to be generated in the first place (so that the sender can determine the datagram that casued the error)

| ICMP Type | Code | Description                     |
| ----------| -----| ------------------------------- |
| 0         |  0   | Echo reply (to ping)            |
| 3         |  0   | destination network unreachable |
| 3         |  1   | destination host unreachable    |
| 8         |  0   | echo request                    |
| 11        |  0   | TTL expired                     |

ICMP messages are used to typically by programs such as traceroute to gather a diagram detailing the network path to a location/server. A packet is initially sent with a TTL of 1, and returns back with the last location that it got to. Then the packet is sent back again, with a TTL of 2, meaning it can hop one more time before it returns. Overtime, this builds the network until it gets to its destination. If that destination is reachable, icmp stops. Otherwise, you will get back a Type 3 code detailing that it is unreachable. 


#### R20. What two types of ICMP messages are received at the sending host executing the Traceroute program?
Generally the sending host will receieve either an ICMP
`Type 3: Destination Unreachable`
or
`Type 11: TTL expired` 

#### P14. Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is no physical link between AS2 and AS4.
* Router 3c learns about prefix x from which routing protocol: OSPF, RIP, 
eBGP, or iBGP?

    `eBGP`
* Router 3a learns about x from which routing protocol?
    
    `iBGP`
* Router 1c learns about x from which routing protocol?
    
    `eBGP`
* Router 1d learns about x from which routing protocol?
    
    `iBGP`

![Alt Text](/week10/Images/P14.JPG)

#### P15. Referring to the previous problem, once router 1d learns about x it will put an entry (x, I) in its forwarding table.
* Will I be equal to I1 or I2 for this entry? Explain why in one sentence.

    `I will be equal to I1 as it is edge location of the next hop`
* Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as via AS3. Will I be set to I1 or I2? Explain why in one sentence.

    `I will be set to I2 because it is a quicker route`
* Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to I1 or I2? Explain why in one sentence.

    `I will be set back to I1 because it will be the path with less hops`

#### P19. In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?
![Alt Text](/week10/Images/Figure5.13.JPG)
> ISP A would advertise to `C` that it has no paths to `W`. This way, `C` would never forward to `A` expecting to get to `W`. 

> ISP A would advertise to both `B` and `C` that it has a path to `V`. Thus both networks would know that it they need to forward to `V`, they could send the information to `A` for routing.

#### P20. Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y’s traffic but does not want to transit X’s traffic. Does BGP allow Z to implement this policy?
>`YES` through the use of filtering/routing policy's, BGP would allow Z to implement this policy.  