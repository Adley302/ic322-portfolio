## ICMP Pinger Lab
>This lab focused capturing packet information through the use of a python program.   
### Process
To complete this lab, I followed the instructions listed on the textbook [ICMP Pinger Lab](https://gaia.cs.umass.edu/kurose_ross/programming/Python_code_only/ICMP_ping_programming_lab_only.pdf) 
### Collab
>I have not finished this lab, I am having difficulties with what bytes are being pulled out of the `struct.unpack()`, i need to go into one of the lab machines to capture the packet that I am sending as running the program through ssh on vscode doesn't populate on my windows wireshark 
