### I can explain the problem that NAT solves, and how it solves that problem.
***Network Address Translation (NAT)*** solves the problem of trying to get ip addresses for all devices in a home network, office, work, and etc. The issue would mainly be adding new devices to your, which would lead to having to potentially request more ip addresses from your ISP. There are not that many IPv4 addresses to begin with, relative to the number of devices on the internet. With NAT however, certain ip ranges can be reused internally within a network. No longer do you have to reach out to your ISP to request more IPs, rather, you have a built internal address range that you can use. The NAT router functions as a normal router when reaching out to the external network and handles all communications between the IP addresses on the local network to external services. You really still only have have a range or a singular IP address, but now all the devices within your internal network can use that singular IP to connect externally, while still having their own specific IPs in the internal network.  
`Internal IP Ranges are:`
>Class A - 10.0.0.0 to 10.255.255.255 (contains up to 16 million unique ip addresses)

>Class B - 172.16.0.0 to 172.31.255.255 (contains about 1 million unique IP addresses)

>Class C - 192.168.0.0 to 192.168.255.255 (contains 65,000 unique IP addresses)

Lets say host A sends a packet with `IP: 192.168.1.1/8432`, when this gets to the gateway router (last stop before leaving the intranet), NAT will change the source ip of the packet into the ip of the gateway router and an arbitrary port number to come back to. Once that is done, the gateway router will then forward said packet to the destined server. The server will see the source as coming from the ip address of the gateway router as opposed to the host behind the NAT. 

### I can explain important differences between IPv4 and IPv6.
The Internet Engineering Task Force began the development of **IPv6** in the early 1990s. The motivation was due to the realization that the 32-bit IPv4 space was beginning to be used up. There is an extensive list of differences.

![Alt Text](/week08/Images/ipv4-vs-ipv6.png)

`Expanded addressing capabilities:` IPv6 increases the size of the IP address from 32 to 128 bits

`Streamlined 40-byte header:` many of the IPv4 fields have been dropped or made optional, allowing for faster processing.

`Notation:` While ipv4 used dotted decimal (192.168.1.1), ipv6 uses hexidecimal notation (2023:odb4:85a3::0000:7358)

`Addressing:` while NAT is used to supplement the unavailability of ipv4 addresses, ipv6 eliminates the need for NAT, allowing each device to have a globally unique address 

`Fragmentation & Checksum:` both of these features were removed from IPv6 due to their complicated nature and redundancies 

There are many more feature differences between ipv4 and ipv6, but those are the most common ones for now. Additional differences include utilizing different protocols in their addressing, transition mechanisms, and security features 
### I can explain how IPv6 datagrams can be sent over networks that only support IPv4.
In order to route an IPv6 datagram over a network that only supports IPv4, the process of **Tunneling** must be used. Tunneling is exactly what it sounds like, it is the idea of masquerading the IPv6 datagram until it reaches it destination. In order to achieve this, the IPv6 datagram, in it entirety, will be put into the payload field of an IPv4 datagram. As the datagram travels from router to router, they will be blissfully unaware that it is an IPv6 datagram until arriving at its IPv6 destination. One it arrives at its destination, the receiving IPv6 node will see tha the `protocol number field` in the IPv4 datagram is **41**, indicating that the IPv4 payload is an IPv6 datagram and will extract it as such. 