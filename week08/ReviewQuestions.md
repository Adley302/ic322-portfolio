#### R23. Visit a host that uses DHCP to obtain its IP address, network mask, default router, and IP address of its local DNS server. List these values.
`Host = My Computer (GNBA-S)`
> **IP Address:** 10.25.164.232

> **Subnet Mask:** 255.255.192.0 

> **Default Gateway:** 10.25.128.1

> **DHCP Server:** 10.1.74.10

> **DNS Server:** 10.1.74.10

#### R26 . Suppose you purchase a wireless router and connect it to your cable modem. Also suppose that your ISP dynamically assigns your connected device (that is, your wireless router) one IP address. Also suppose that you have five PCs at home that use 802.11 to wirelessly connect to your wireless router. How are IP addresses assigned to the five PCs? Does the wireless router use NAT? Why or why not?
**Yes**, the wireless router uses NAT in order to connect your five PCs to the outside network. The IP addresses themselves, without configuring the router, will be dynamically allocated to each device as they connect to the router using DHCP. There will only be one IP actually leaving the home network however to connect to the internet. The NAT table will handle the address translation of the incoming and outgoing connections to external networks. 

#### R29. What is a private network address? Should a datagram with a private network address ever be present in the larger public Internet? Explain.
A private network address is a range of IP addresses set aside for the sole use on internal networks. These ranges are classified into 3 groups. `Class A (10.0.0.0)`, `Class B (172.16.0.0)`, and `Class C (192.168.0.0)`. A datagram with a private network address should never be present in the larger public internet because your NAT should translate it prior to sending it out. It could still be possible to see it, but most routers and networks are configured to block those IP ranges if coming in from the external network. 

#### P15. Consider the topology shown in Figure 4.20. Denote the three subnets with hosts (starting clockwise at 12:00) as Networks A, B, and C. Denote the subnets without hosts as Networks D, E, and F.
![Alt Text](/week08/Images/figure_420.JPG)
* #### a. Assign network addresses to each of these six subnets, with the following constraints: All addresses must be allocated from 214.97.254/23; Subnet A should have enough addresses to support 250 interfaces; Subnet B should have enough addresses to support 120 interfaces; and Subnet C should have enough addresses to support 120 interfaces. Of course, subnets D, E and F should each be able to support two interfaces. For each subnet, the assignment should take the form a.b.c.d/x or a.b.c.d/x – e.f.g.h/y.
    **Subnet A:** 214.97.254.0/24 (can support 250 interfaces)

    **Subnet B:** 214.97.255.0/25 (can support 120 interfaces)

    **Subnet C:** 214.97.255.128/25 (can support 120 interfaces)

    **Subnet D:** 214.97.254.2/31 (last bit can either be a 1 or 0; aka 2 interfaces)

    **Subnet E:** 214.97.254.4/31 (last bit can either be a 1 or 0; aka 2 interfaces)

    **Subnet F:** 214.97.254.6/31 (last bit can either be a 1 or 0; aka 2 interfaces)


* #### b. Using your answer to part (a), provide the forwarding tables (using longest prefix matching) for each of the three routers.
    `Router 1:`
    | Destination | Interface | 
    | ---------------- | ----------- | 
    | 214.97.254.0/23 | 0 | 
    | 214.97.254.2/31 | 1 | 
    | 214.97.254.4/31 | 2 | 
    | 214.97.254.6/31 | 3 | 

    `Router 2:`
    | Destination | Interface | 
    | ---------------- | ----------- | 
    | 214.97.255.0/25 | 0 | 
    | 214.97.254.128/25 | 1 | 
    | 214.97.254.2/31 | 2 | 

    `Router 3:`
    | Destination | Interface | 
    | ---------------- | ----------- | 
    | 214.97.255.2/31 | 0 | 
    | 214.97.254.130/31 | 1 | 

#### P16. Use the whois service at the American Registry for Internet Numbers (http://www.arin.net/whois) to determine the IP address blocks for three universities. Can the whois services be used to determine with certainty the geographical location of a specific IP address? Use www.maxmind.com to determine the locations of the Web servers at each of these universities.
`University of Central Florida(UCF):`
Net Range 132.170.0.0 - 132.170.255.255  **&&** 
Net Range 192.31.108.0 - 192.31.108.255

`Louisiana State University(LSU):`
Net Range 192.16.176.0 - 192.16176.255  **&&** 
Net Range 76.165.224.0 - 76.165.255.255

`Auburn University:`
Net Range 131.204.0.0 - 131.204.255.255

The IP address can be used to determine the geographical location, but nothing more personal than that.  

#### P18. Consider the network setup in Figure 4.25. Suppose that the ISP instead assigns the router the address 24.34.112.235 and that the network address of the home network is 192.168.1/24.
![Alt Text](/week08/Images/figure_425.JPG)
* #### a. Assign addresses to all interfaces in the home network.
| Interface | IP Address | 
| ---------------- | ----------- | 
| 1 | 192.168.1.1 | 
| 2 | 192.168.1.2 |
| 3 | 192.168.1.3 |
* #### b. Suppose each host has two ongoing TCP connections, all to port 80 at host 128.119.40.86. Provide the six corresponding entries in the NAT translation table.
    | External | Internal | Host |
    | ---------------- | ----------- | ----| 
    | 24.34.112.235, 5001 | 192.168.1.1, 3345 |  1  | 
    | 24.34.112.235, 5002 | 192.168.1.1, 3445 |  1  |
    | 24.34.112.235, 6772  | 192.168.1.2, 4445 |  2  |
    | 24.34.112.235, 6457  | 192.168.1.2, 4554 |  2  |
    | 24.34.112.235, 5242  | 192.168.1.3, 2546 |  3  |
    | 24.34.112.235, 5899  | 192.168.1.3, 2648 |  3  |

#### P19. Suppose you are interested in detecting the number of hosts behind a NAT. You observe that the IP layer stamps an identification number sequentially on each IP packet. The identification number of the first IP packet generated by a host is a random number, and the identification numbers of the subsequent IP packets are sequentially assigned. Assume all IP packets generated by hosts behind the NAT are sent to the outside world.
* #### a. Based on this observation, and assuming you can sniff all packets sent by the NAT to the outside, can you outline a simple technique that detects the number of unique hosts behind a NAT? Justify your answer.
    > In this particular case, one could make a code to run through a list of identifation numbers as they come up, each time the identification does not increment by 1, we can assume that it is new host. This does not promise identifying all unique hosts on the NAT, but it gives an estimate based on this particular situation. 

* #### b. If the identification numbers are not sequentially assigned but randomly assigned, would your technique work? Justify your answer.
    > My technique written in part one would not work if the identification numbers were randomly assigned. 