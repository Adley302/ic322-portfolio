## Protocol Pioneer Lab
>For this lab, i completed chapters 1-4 of protocol pioneer. The game involved brought to the forefront the reality of how much case by case comparisons it takes to implement a routing table. 
### Process
To complete this lab, I followed the instructions listed on the [Gitlab](https://gitlab.com/jldowns-usna/protocol-pioneer) as well as the instructions on the running Jupyter notebook itself.
### Collab
NONE
### Questions
### How did you solve Chapter 1? Please copy and paste your winning strategy, and also explain it in English.
```
if self.state["ticks"] % 6 == 0:
    self.send_message("Message Received", "W")
```
* In plain english, this code waits for the amount of ticks (6) it takes the received the signal from the ship, and in response, it messages back 'Message Received' on the correct interface 'W'.
### How did you solve Chapter 2? Again, copy and paste your winning strategy and explain it.

![Alt Text](/week08/Images/Chapter2.JPG)
>For my winning code to chapter 2 reads:
>
>each message I receive, add the value received to the interface and increment the count of number received for that interface by one. When an interface has received three values, send the summation back to the associated ship.

### How did you solve Chapter 3? Please copy and paste your winning strategy, and also explain it in English.
> ![Alt Text](/week08/Images/Chapter3.JPG)
 * My solution for chapter 3 takes in the input and does one of two things.
    * performs the provided command if it is the correct drone **OR**
    * passes it along the rail to the direction of the next drone

### How did you solve Chapter 4? Please copy and paste your winning strategy, and also explain it in English.
`Drone Strategy:`
* **Step 1)** Split the receiving message into variables containing only respective values 
* **Step 2)** Divide the code by **dir** which details whether the packet is coming or going
* **Step 3)** dir == 2, means its going to a scanner, so route accordingly to a respective scanner direction
* **Step 4)** else (aka dir == 1), means its going back to an analyzer, so route the packet accordingly to respective analyzer

![Alt Text](/week08/Images/drone_strategy.JPG)

`Scanner Strategy:`
* **Step 1)** Split the receiving message into necessary variables we need 
* **Step 2)** boot the incoming value and save it to result
* **Step 3)** send back the message in the same receiving format so that drones can properly split the information and forward it back to analyzer

![Alt text](/week08/Images/scanner_strategy.JPG)

### Include a section on Beta Testing Notes.

* List any bugs you find. When you list the bugs, try to specify the exact conditions that causes them so I can reproduce them.
    > I did not personally find any bugs with the program
* Was there anything that you and others were consistently confused about? What would you do to solve that confusion?
    > Getting used to the commands took a bit of time, but afterwards it was just a thinking game 
* Was there too much narrative? Not enough narrative?
    > I think the narrative was okay, I didn't read the entire narratives as I played however, more just looked for specific information on what to do and if i succeeded.
* Was it fun?
    > It started getting more fun toward the end of chapter 3 when i understand the general functions and how everything was going to be processed
* Really, just let me know how I can improve the game.