### I can explain what a subnet is, how a subnet mask is used, and how longest prefix matching is used to route datagrams to their intended subnets.
1. a subnet mask works as a subdivision of an IP network. subnets are used to divide larger networks into smaller, more manageable chunks. subnet masks are notated in the same 32 bit number sequence as an IP address. The subnet mask that tells you which portion of the ip segment is for hosts and which portions for the network. subnetting allows you to isolate parts of the network from each other in order to provide better security and enhance network performance. 
2. When a packet/datagram comes into a router or input port, a router can direct it to the proper output/direction by using longest prefix matching. In order to do this, the router uses the routing table that it contains and the entries specifying the destination IP address ranges as well as the next hop location. 
    * IP datagram arrives 
    * Router performs a lookup in the routing table to find the best match for the destination IP
    * Router selects the next hop location associated with the longest matching prefix 
### I can step though the DHCP protocol and show how it is used to assign IP addresses.
`Dynamic Host Configuration Protol` allows a host to obtain (be allocated) an IP address automatically. A network administrator can cofigure DHCP so that a given host receives the same IP addres each time it connects to the network, or a host may be assigned a **temporary IP address** that will be different each time the host connects to the network.

![Alt Text](Images/DHCP.JPG)

1) Host sends a **DHCP discover message** within a UDP packet to port 67 (packet is encapsulated in an IP datagram)
2) DHCP client (router) creates an IP datagram containing DHCP discover message along with the broadcast destination IP address of 255.255.255.255 and a source IP address of 0.0.0.0. This frame is **broadcasted to all nodes** attached to the subnet.
3) DHCP server receives the discover message and replies back with a **DHCP offer message** to all node in the subnet.
4) The client will then select from one or more server offers and respond with a **DHCP request message** containing the configuration parameters.
5) The server then responds to the DHCP request message with a **DHCP ACK message.**