--This week's questions are from chapter 4--


### R11. Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers)?
`What is an input port:` an input port is a physical device or logical interface designed to receive data or signals from external devices. some examples are input devices are routers, switches, computers, sensors, and network printers.

![Alt Text](Images/Router.JPG)
#### To best understanding how packet loss can occur at input ports, first take a look at this picture. This picture illustrates what inside of a router.

>Packet loss will typically occur at an input port when the switch fabric, detailed in the picture above, is not fast enough (relative to the input line speeds) to transfer all arriving packets through the fabric without delay.
* Take for example if two packets at the front of two input queues are destined for the same output queue, then one packet will be blocked and must wait for the other to be transfered first.
* Additionally, you can have Head of Line Blocking (which will be covered down below) where a packet is blocked waiting for the packet ahead of it to be transfered.
>Overall, when the block packets start to build up, the input port will begin running out of buffer space. When buffer space is depleted, the router must make the decision to drop or replace packets currently in the buffer.


Packet loss can be **mitigated** as input ports by using preventative measures as well as choice of transfer protocol:

* Option one would be to use TCP over UDP which has built in flow control mechanisms to mitigate congestion and buffer overflow as we discussed in previous classes.
* upgrading the network infrastructure and bandwidth can also be used to handle more packets at a time. The overall idea of this is to reduce the overall transmission and queue delay to handle the influx of packets
* finally, in bigger networks, it is possible to implement buffer sharing to store more packets while waiting for transmission.

### R12. Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?
Similar to input ports, packet loss can occur at output ports. 
>As packets are being pushed in through the input ports and forwarded over by the switch fabric at X speed, they can be still queued if the output port has not yet finished transmitting all the packets currently in its buffer. Irregardless of the switch fabric speed, packets in the buffer of the output port must still wait for transmission over the outgoing link of the packet ahead of them. 

### R13. What is HOL blocking? Does it occur in input ports or output ports?
`HOL Blocking: ` is the phenomenon where a queued packet in the **input queue** must wait for transfer through the fabric (even if its outport port is free) because it is blocked by another packet at the in front of it. HOL blocking can cause the input queue to grow to unbounded lengths...aka significant packet loss.

### R16.  What is an essential different between RR and WFQ packet scheduling? Is there a case (Hint: Consider the WFQ weights) where RR and WFQ will behave exactly the same?
Round Robin (RR) and Weighted Far Queuing (WFQ) are two different packet scheduling algorithms used in networking to manage the transmission of data from multiple sources.

`Round Robin: ` a round robin scheduler alternates service among the classes. If there are 3 classes of packets. Round Robin would scehdule class 1 ---> class 2 ---> class 3 ----> class 1 --- repeat. Key thing to note is that a class is a description of a type of packet and not a packet itself. RR will alternate cycles unless the next class is not available when it is free to transmit, it which case it moves on to the next. 
>* Each queue gets an equal opportunity to transmit data
>* Each queue is given an equal share of the available bandwidth
>* Very predictable service for all queues
>* Simple implementation

`WFQ; ` Weighter Far Queuing scheduler does the same task as RR but weight can added to a class to increase the number of times per complete cycle, a particular class gets transmitted. ie a circular manner.
>* scheduler allocates bandwidth based on weights
>* allows for prioritization of certain queues
>* Also takes into account the size of packets when determining service order
>* More suitable for networks with diverse traffic patterns

**Similar behavior:** The case in which RR and WFQ will act the same is when the weights off all classes in WFQ are equal. 

### R18.  What field in the IP header can be used to ensure that a packet is forwarded through no more than N routers?
The `Time-to-live` (TTL) field can be used to ensure that datagrams do not circulate forever. TTL decreases by one each time it is forwarded, meaning if we set the TTL to N, then it will dropped after being forwarded N times. 

### R21. Do routers have IP addresses? if so, how many??
#### Yes, routers have multiple IP addresses.
1. **LAN:**
    * This IP address is used by hosts on the internal network to communicate to the router
2. **WAN:**
    * Used by the router to communicate with other devices outside of the local network
3. **Management:**
    * Used by network administrators in order to to access and configure the router 
4. **Interface:**
    * The subnet ranges that the router uses in the forwarding table, not for the router itself.
5. **Virtual:**
    * In some cases, the router may also have virtual and loopback addresses for the internal network

Problems Section:
### P4.  Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?
![Alt Text](Images/P4.JPG) 

1. Minimal time slots needed is **3** due to HOL Blocking
2. Worst case scenario is also **3** because a non-empty input queue is never idle

### P5. Suppose that the WEQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.
#### a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .).
>112311231123...
#### b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?
>112112112...
### P8. Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows:
![Alt Text](Images/P8a.JPG)
#### a. provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces
|Prefix Match | Interface | 
| ---------------- | ----------- | 
| 11100000 00 | 0 | 
| 11100000 010000000 | 1 | 
| 11100000 010000001 | 2 | 
| 11100001 0 | 2 | 
| otherwise | 3 |
#### b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses:
![Alt Text](Images/P8b.JPG)
```
11001000 10010001 01010001 01010101 -----> Interface 3
11100001 01000000 11000011 00111100 -----> Interface 2
11100001 10000000 00010001 01110111 -----> Interface 3
```


### P9. Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table:
|Prefix Match | Interface | 
| ---------------- | ----------- | 
| 00 | 0 | 
| 010 | 1 | 
| 011 | 2 | 
| 10 | 2 | 
| 11 | 3 |

#### For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.
| Interface | Range | # of Addresses |
| ---------------- | ----------- | --- |
| 0 | 00000000 - 00111111 | 64 |
| 1 | 01000000 - 01011111 | 32 |
| 2 | 01100000 - 01111111 | 32 |
| 2 | 10000000 - 11111111 | 64 |
| 3 | 11000000 - 11111111 | 64 |

### P11. Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table:
|Prefix Match | Interface | 
| ---------------- | ----------- | 
| 1 | 0 | 
| 10 | 1 | 
| 111 | 2 | 
| otherwise | 3 | 

#### For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.
| Interface | Range | # of Addresses |
| ---------------- | ----------- | --- |
| 0 | 10000000 - 11111111 | 128 |
| 1 | 10000000 - 10111111 | 64 |
| 2 | 11100000 - 11111111 | 32 |
| 3 | otherwise | # |

