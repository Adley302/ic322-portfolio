## WireShark IP Lab
>This lab was about focused on exploring the IP protocol by analyzing a trace of IP datagrams on wireshark. Using Traceroute, I was able to do into the ip and icmp packets to see what contents were stored in their headers and fields.
### Process
To complete this lab, I followed the instructions listed on the Wireshark Lab version 8.1 [website](https://gaia.cs.umass.edu/kurose_ross/wireshark.php) 
### Collab
NONE
### Questions
#### 1. What is the IP Address of your computer?
192.168.1.102
#### 2. What is the value in the upper layer protocol field?
ICMP
#### 3. How many bytes are in the IP header
Header length = 20 bytes
Total length = 84 bytes
Payload length = 84-20 = 64 bytes
#### 4. Has this IP datagram been fragmented? Explain
The datagram has not been fragmented. Determined by looking at the `More fragments` field and `Fragment offset`
#### 5. Which fields in the IP datagram always change from one datagram to the next within this series of ICMP messages sent by your computer?
* Header Checksum
* Time to Live
* Identification 
#### 6. Which field stay constant? Which of the fields *must* stay constant? Which fields must change? why?
**Must stay constant:** 
* Header length, because that is the required byte size of header
* Protocol stays the same because its what we are using ICMP
* IP version 4 must stay constant 
* Differientiated service field 
* Source IP address for outbound pings
**Must change:**
* Header checksum must change
* Identification must change 
* Time-to-live because it increments with each packet
#### 7. Describe the pattern you see in the values in the Identification field of the IP datagram.
>For each Echo (ping) request we send out, the identification field increments by 1.

#### 8. What is the value in the Identification field and the TTL field

`Identification:` 0x9d7c (40316) 

`TTL:` 255

#### 9. Do these values remain unchanged for all of the ICMP TTL-exceeded replies sentto your computer by the nearest (first hop) router? Why?
`Yes:` they remained unchanged because the TTL of the first hop router will always be the same. 

#### 10. Find the first ICMP Echo Request message that was sent by your computer after you changed the Packet Size in pingplotter to be 2000. Has that message been fragmented across more than one IP datagram?
`Yes:` the packet has been fragmented across two datagrams 

#### 11. . Print out the first fragment of the fragmented IP datagram. What information in the IP header indicates that the datagram been fragmented? What information in the IP header indicates whether this is the first fragment versus a latter fragment? How long is this IP datagram?
![Alt Text](Fragment1.JPG)
`Flag Field:` Tells us that there is 1 more fragment set. Additionally the **fragment offset of 0** tells us that this is the first fragment.

`IP Length = 1500` 

#### 12. Print out the second fragment of the fragmented IP datagram. What information in the IP header indicates that this is not the first datagram fragment? Are the more fragments? How can you tell?
![Alt Text](Fragment2.JPG)
The **Fragment offset: 1480** tells you that this is not the first fragment.
The **More fragment: Not set** tells you that this is the last fragment.

#### 13. What fields change in the IP header between the first and second fragment?
Fields that change:
* Total Length
* More Fragments 
* Header checksum

#### 14. How many fragments were created from the original datagram?
**Three** fragments were created from the original datagram 

#### 15. What fields change in the IP header among the fragments?
* Total Length
* Final fragment does not have the Flag set
* Header Checksum
