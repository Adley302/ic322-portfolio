## R1. What is the difference betweeen a host and end system? List several different types of end systems. Is a Web server an end system?

    A host and end system are the same thing, they are the devices that make up the internet of things. Some examples of different types
    of end systems are computers, laptops, phones, tablets, and web server. They are not the routers that direct traffic but rather, 
    the devices that exist on the network edge through which the users interact with.  

## R4. List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access.

- Digital Subscriber Line (DSL) : Home Access
- Ethernet : Enterprise and Home Access
- 4G LTE : Wide-Area wireless access
- 5G : Wide-Area wireless access 

## R11. Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2, respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queuing, propagation delay, and processing delay.)

    2L/R where L is the length of the packet and R is the speed of the used medium in Mbps.

## R12. What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?

    With a circuit-switched network, the resources needed along communication paths are reserved between the end systems, thus no queue is developed.
    With a TDM in a circuit-switched network, for the duration of the connection, each circuit will get all the bandwidth.

## R13. Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20 percent of the time. (See the discussion of statistical multiplexing in Section 1.3.)

1.  When circuit switching is used, how many users can be supported?
    only two users can be supported at a time with a 1 Mbps reserved slot for each
2.  For the remainder of this problem, suppose packet switching is used. Why
    will there be essentially no queuing delay before the link if two or fewer
    users transmit at the same time? Why will there be a queuing delay if
    three users transmit at the same time?
    With two or fewer users, the 2 Mbps link can handle all the data transmittion without needing to congest or delay. When a third user is added
    then the data will need to be split among all 3, causing a queue to build. 
3.  Find the probability that a given user is transmitting.
     *** 
4.  Suppose now there are three users. Find the probability that at any given
    time, all three users are transmitting simultaneously. Find the fraction of
    time during which the queue grows

## R14. Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?

    Two ISPs at the same level often peer with each other so that their traffic can go through a direct communication, 
    rather than upstream intermediaries. When peered together, the cost is typically settlement free as opposed to the 
    cost that would have to be paid for the services received from a next level ISP. 

## R18. How long does it take a packet of length 1,000 bytes to propagate over a
link of distance 2,500 km, propagation speed 2.5 * 10^8m/s, and transmission
rate 2 Mbps? More generally, how long does it take a packet of length L to
propagate over a link of distance d, propagation speed s, and transmission rate
R bps? Does this delay depend on the packet length? Does this delay depend on 
transmission rate. 
* The series of questions confuses me but the overall delay of the packet to get from one host to the other 
  can be calculated as such:
  ** buffer delay (if any packets are currently being sent to the link) +
  ** transmission delay; in this case L/R or Length of packet / transmission rate bps +
  ** propagation delay; in this case speed 2.5e^8 / distance 2500 
* you sum up the total and you get your full delay for the packet transfer. the delay depends on both packet length and transmission rate

## R19. Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps.
a. Assuming no other traffic in the network, what is the throughput for the
file transfer?
* the throughout for this file transfer is only as fast as the slowest link, meaning 500kbps

b. Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B?
* 4,000,000 / 500,000 = roughly 8 seconds for the file to transfer to host b 

c. Repeat (a) and (b), but now with R2 reduced to 100 kbps.Yup, your virtual machine crashed and burned!