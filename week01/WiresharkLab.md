#### List 3 different protocols that appear in the protocol column in the unfiltered packet-listing window in step 7 above.
 When running through this lab, I spent much more time capturing packets than I feel was probably intended for the work. The different protocols I was able to observer were HTTP, TCP, NFS, TLSv1.2, LLDP, SSDP, MDNS, and ICMPv6. 

####  How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received? (By default, the value of the Time column in the packetlisting window is the amount of time, in seconds, since Wireshark tracing began. To display the Time field in time-of-day format, select the Wireshark View pull down menu, then select Time Display Format, then select Time-of-day.)
* The GET request was sent out at 11:25:49.5821
* The Ok response was received at 11:25:49.6023
* Overall it looks about .02 seconds.


#### What is the Internet address of the gaia.cs.umass.edu (also known as wwwnet.cs.umass.edu)? What is the Internet address of your computer?
The Internet address (IP) associated with gaica.cs.umass.edu is 128.119.245.12

#### Print the two HTTP messages (GET and OK) referred to in question 2 above. Todo so, select Print from the Wireshark File command menu, and select the “Selected Packet Only” and “Print as displayed” radial buttons, and then click OK.
* Located in `WiresharkLab_GET2 file`