## I can explain the role that the network core plays vs the network edge.
    The network edge consists of those devices that sit at the edge of the Internet known as hosts or end systems.
    Examples of these are computers, laptops, smartphones, tablets, and etc. These systems are essential because
    they also host important programs on internet such as browsers, servers, emails, and the sorts. The network edge
    serves as our physical access to the internet, they are the devices that we use, and the visuals that we actually see.

    The network core is the behind the scenes mastermind that allows for the interconnected systems that make up the internet.
    The core consists of packet switches and links that carry information from one end system to another. It is not as visual 
    as the network edge, but its importance is uncontested as it provides the visual that you do see when accessing the internet.

## I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.
    To begin, an access network is the network that physically connects an end system on the network edge to the last router
    prior to arriving at the network core. This is typically your home router or gateway that stands guard between you and 
    the rest of the traffic on the internet. They work to secure the network edge and protect the core through firewalls and 
    characterization. 
    DSL was obtained through the same local telephone company. the home dsl moden would translate the digital data into a high-frequency
    tone over the phone wires to the central office where it would be translated back to digital at the DSLAM  where it would head over to
    the network core to process internet requests. The frequencies were carried over telephone wires (twisted copper).
    
    Cable required special modems, called cable modems. The cable head end served a similar function as the DSL networks DSLAM turning
    analog signal from modems back in digital format. Using coxial cable lines, they connect to the fiber nodes in the neighborhood which
    connect to the CMTS through a fiber connection where the internet requests can be sent out to the network core. 

    FTTH (Fiber to the home) was a breakthrough in thus that it allowed direct high speed connection to the CO. each fiber leaving the CO
    is shared by many homes, but when it gets close to the neighborhoods it splits into the separate homes. Each home would be equipped with
    an optical network terminator (ONT) which is connected by dedicated optical fiber to a neighborhood spliter, this splitter then connects
    directly to the line at the CO.

    The age of Wireless connectivity brought the innovation of not needed costly installations of cables from the home to telco's CO. Instead,
    with the use of beam-forming technology, data could be sent wirelessly to a providers base station, then to a modem in the home which is
    now bundled with a WIFI wireless router that your end system would be connected to. 
    
## I can use queuing, transmission, and propagation delays to calculate total packet delay, I can describe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.

**Queuing delay:** Following the initial processing delay from the upstream node to the first router, the queuing delay is the first of the three to be experienced. The length of this delay mainly depends on the number of packets that are already queued and waiting to go, as the packets will be transmitted onto the link in the order that they arrived. These delays take on the order of micro-milli seconds. The queuing delay can only be improved with the help of the other two, to speed up the process for any packets ahead of you. The transmission delay can be improved by getting a faster ethernet link or network media. propagation delay can be improved by shortening the distance between the two routers and the physical medium link itself.

**Transmission delay:** similar to the queue delay, our packet can only be transmitted once all the packets that have
arrived before it have been transmitted. The delay of the transmission is based on L (the length of the packet in bits) and R (the transmission rate of the link from the source to destination). The math adds up to be L/R or bits/sec. to note R is received from a link such as an Ethernet link which might be a 100Mbps. You take that 100 as R and L being x number of bits and say that the transmission delay would equal the total number of bits needed to transmitted (L)/ the speed at which the link can transmit the information (R). aka L/R. 

**Propagation Delay:** Lastly we have the propagation delay which factors the actual distance between the two routers that the packet is travelling to and the physical medium of the link (fiber, copper, and etc). The speeds of the physical mediums themselves are generally equal to or close to the speed of light. to calculate propagation delay you divide d (distance between routers)/ s (propagation speed). Important distinction analogy to distinguish all the delays, imagine that you're going to a dealership to rent a car but have no reservation. The queue delay would be how long you had to wait in line before getting to speak to the attendee to get a vehicle. the transmission delay would be the length of time it takes for everyone to load into the car and strap on the seatbelt. The propagation delay would be the travel time your gps says it will take to get to your intended destination now that you're on the road.


## I can describe the differences between packet-switched networks and circuit-switched networks.
    The delays I referred to above were relating to a packet-switched network, ie. a network that allows for multiple simultaneous connections,
    with the consideration that there will be wait times, delays, and etc. 
    A circuit switched network on the other hand, the resources needed such as buffers and link transmission rate for the communications are reserved
    for the duration of the session between two end systems, thus limited the total number of connections to a fixed number. 
    Some examples of where one might be better than the other would include telephone and video calls, In these instances you would want a full connection
    at all times so that you can understand the person, ie a circuit-switched network. On the other side of the spectrum however, lets say most other things 
    it makes very little sense to not have packet-switched network to allow for multiple connections at once. 

## I can describe how to create multiple channels in a single medium using FDM and TDM.
    Frequency-division multiplexing (FDM) is when a circuit in the link is divided among the connections established across the link. Each connection is
    dedicated a frequency band for the duration of the connection. With that, each circuit continuously gets a fraction of the bandwidth. For example if
    we had medium split among 3GHz, with 3 separate hosts each getting assigned 1GHz, the 3 hosts would split the medium by 3 and be able to all transmit
    at the same time. 
    Time-division frequency (TDM) is when each frame is divided into a fixed number of time slots. When a connection is established, the network dedicates
    one time slot in every frame to the new connection. Each slot is dedicated for the sole use of said connection, ie they would get a full 3GHz, but only
    during their time slot. 

## I can describe the hierarchy of ISPs and how ISPs at different or similar levels interact.
    Internet Service Providers (ISPs) make up the network of networks that allow for communication between the network edge and network core, they allow
    for access to the broader internet. At the bottom of the pole you have local or access ISPs that are connected to regional ISPs. As a result whenever 
    upstream traffic is necessary, these local ISPs must pay the regional ISPs for the traffic through their network. The same works for when a regional
    ISP must route traffic upstream to a Tier 1 ISP who has a bigger scope of connectivity. With that being the case, ISPs at the same level often times
    peer with one another, allowing them to share traffic directly without the need to go upstream. The benefit of peering is that they don't have to pay 
    one another, the relationship is mutually shared between the two of them. Additionally there exists IXPs or Internet Exchange Points from third parties 
    which work as hubs for different ISPs to meet at a place in order to peer with one another.   

## I can explain how encapsulation is used to implement the layered model of the Internet.
    Encapsulation allows for the division of an otherwise complex system of communication. The layered model separates each part of the information routing
    into manageable layers, each being responsible for specific functions, as well as incorporating theyre own routing, error, and function checks. Starting 
    at the end system or host, you have layer 5, the application layer, which stores the information (typically the more user readable friendly content you
    wish to send) and destination ip & port it needs to go. After that, it is sent to the transport layer which reads where the message wants to go and adds 
    its own header to format the message. That is then sent to the network layer who reads the message, looks at the ip address and adds its own header
    to adjust and prepare the packet. This process goes down the line until it reaches the physical layer, where it travels to its destination. Once arriving 
    the packet is then unassembled in the reverse process each layer at a time until it finishes back up at the application layer to the intended end system.
