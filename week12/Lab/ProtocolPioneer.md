## Protocol Pioneer Act II
> The purpose of this lab is to figure out a way to maximize the steady-state success rate of nodes transmitting information on the same broadcast channel. 

### Process
I followed the instructions guide on Protocol Pioneer Act II as well as looking over the textbook at the ALOHA and CSMA/CD protocol.

### Collaboration
`Partner:` Peter Asjes

### Server Strategy:
```
def server_strategy(self):
    
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0]
    if not self.interface_sending(selected_interface):
        if len(self.from_layer_3()) > 0:
            if self.current_tick()%16 == ((int(self.id)-2)*4-2) or self.current_tick()%16 == ((int(self.id)+2)*4-2):
                if not self.interface_receiving(selected_interface):
                    msg_text = self.from_layer_3().pop()
                    print(f"server {self.id}: Attempting send to: " + msg_text)
                    self.send_message(msg_text, selected_interface)
                else:
                    self.cancel_interface(selected_interface)
```
My server strategy composed of the following elements:
* `t=2` ---> server 3 can send
* `t=6` ---> server 4 can send
* Timer ranged from 0 - 15
* If it was time to send but the server was currently receiving information, then it would wait and try again the next time its block came up. 

### Client Strategy:

```
def client_strategy(self):
    
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0]
    if not self.interface_sending(selected_interface):
        if len(self.from_layer_3()) > 0:
            if self.current_tick()%16 == ((int(self.id)-1)*4) or self.current_tick()%16 == ((int(self.id)+1)*4):
                if not self.interface_receiving(selected_interface):
                    msg_text = self.from_layer_3().pop()
                    dest = msg_text.split(",")[1].split(":")[1]
                    print(f"client {self.id}: Attempting send to: {dest} : current tick is {self.current_tick()%16}")
                    self.send_message(msg_text, selected_interface)
                else:
                    self.cancel_interface(selected_interface)

        if self.interface_receiving(selected_interface):
            print("i canceled the send")
            self.cancel_interface(selected_interface)
```

My client strategy composed of the following elements:
* Timer ranged from 0 - 15
* `t = 0 or 8` client 1 would send its information (at the top of the timeblock only)
* `t = 4 or 12` client 2 would send its information
*  Clients would only send if they were not currently receiving a transmission at the top of the timeblock
* if a message was currently being received, they would cancel sending their own

### Below are some statistics from our code
![Alt Text](/week12/Images/PP4.JPG)

![Alt Text](/week12/Images/PP3.jpg)

![Alt Text](/week12/Images/PP2.jpg)

#### <ins>**Overall:**
I think the strategy does moderately okay with the constraints for what we could access. No two runs are really the same for this, it really depended on who talked first that drove the final average. I would like to implement a strategy in the future where I could check to see if there was any traffic on the channel prior to sending. 