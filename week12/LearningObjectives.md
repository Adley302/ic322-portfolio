### I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem.
The multiple access problem is the issue that arises when dealing with how to coordinate the access of multiple sending and receiving nodes on a shared broadcast channel. This problem operates at the link-layer. I will be detailing how this problem is approached in the link-layer two multiple-access protocols; ALOHA & CSMA/CD.

Operating out of the data link layer, the **ALOHA** protocol was the first multiple-access protocol, allowing geograhically distributed users to share a single broadcast medium. There are two variants to the ALOHA protocol. Slotted ALOHA and Pure ALOHA.

![Alt Text](/week12/Images/Slotted_ALOHA.JPG)

**SLOTTED ALOHA:**
* All frames consist of exactly L bits
* Nodes start to transmit frames only at the beginning of the slots
* If two or more frames collide in a slot, then all the nodes detect the collision before the slot ends
* Time is divided into slots of L/R seconds (aka the time to transmit exactly one frame)
* If a collision occurs, the node retransmits the frame in each subsequent slot with probablity p until the frame is transmitted without a collision

Slotted ALOHA works really well when there is only a single node transmitting but not as well when there is multiple node. However, it achieves the ability to have multiple nodes sending and receiving on a shared broadcast channel. The effectiveness of this is based on the number of nodes. The calculation Np*(1-p*)^N-1 as N approaches infinity solves for the maximum efficiency when a large number of nodes have many frames to transmit. After the calculation, the arriving number if `37%` of the slots achieve a successful transmissions. 

![Alt Text](/week12/Images/Pure_ALOHA.JPG)

**PURE ALOHA:** 

the first aloha protocol was a pure aloha, slotted came afterwards
* when a frame came down from the network layer, it would be transmitted immediately in its entirety to the broadcast channel
* If collision is detected, the frame will be transmitted with probability P (with t=0 starting after the collided frame has finished transmitting)

Pure ALOHAs effectiveness therefore is based the on the chance that no frames are being transmitted within the time frame that another node is currently sending a frame across the broadcast channel. This probablity (assuming out transmission time to be a unit of time) can be calculated with p(1-p)^2(N-1). Which, when taking the limit to infinity, gives us a maximum efficiency of roughly `18.5%`, which is half that of Slotted ALOHA. 

**Carrier Sense Multiple Access with collision detection (CSMA/CD):** embodies two rules that ALOHA skips over entirely. 

![Alt Text](/week12/Images/CSMA-CD.JPG)

1. `carrier sending:` listening to the channel before transmitting and waiting until it detects no transmissions for a while before starting its transmission. 
2. `collision detection:` the node listens to the channel while transmitting, and if another node is detected to be transmitting, it stops transmitting and waits a random time before activating carrier sending cycle.

* The random time wait before attempting to send again prevents two frames who collided from attempting to resend at the same time. 


### I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC.
<ins>**`Parity Checks:`**</ins>

single parity bits are the simplest form of error detection. taking the information to be sent as `d-bits` in length, the total number of bits are going to be sent will be d+1. With that there are two types of singly parity checks. `even parity` and `odd parity`. 

![Alt Text](/week12/Images/Even_bit_parity.JPG)

**Even Parity:**
* counts the number of 1s in `d`, if that number is odd... the added parity bit will be 1 in order to make the 1s count even. If the number of 1s in `d` is an even number, the added on parity bit will be a 0, in order to maintain an even number of 1s. 

**Odd Parity:**
* odd parity works in the same fashion an even parity, but the goal of the parity bit to maintain an odd number of 1s as opposed to an even number. 

**Fault of single-bit parity**:
* With single bit parity, you can easily detect when an odd number of bits have been flipped, but if that number is even... when the receiving side checks the parity bit, it will not be able to detect in anamoly in the message. Because errors tend to cluster together, the probability of an undetected error in a frame can therefore approach upwards of 50%.

<ins>**`2-D Parity Checks:`**</ins>

![Alt Text](/week12/Images/2D-Parity.JPG)

2-D parity checks give a two-dimensional generalization of the single bit parity scheme. the `d` bits are divided into rows and columns. The greatest strength of the 2-D parity check is its ability to not only detect a single bit error, but also flip it back to its intended bit.
* 2-D parity can also detect (**but not correct**) any combination of two errors in a packet.

<ins>**`Internet Checksum:`**</ins>

![Alt Text](/week12/Images/internet-checksum-l.jpg)

The internet checksum is a simple error detection mechanism that helps to provide a level of protection against common transmission errors. 
**How it works**
1. The data is divided into 16-bit chunks
2. The 16-bit values are then summed together (with the overflows being added back to the sum)
3. The one's complement of the final sum is calculated and stored in the checksum field of the packet
4. On the receiver side, the 1s complement of the sum of the received data (including checksum) is taken
5. If the result is all 0s - then there is no error. If any of the resulting bits are 1s, an error is indicated.

Internet Checksum `occurs at the transport layer`. In TCP and UDP protocols, the checksum is computed over all the fields. in IP, it is computed over the IP header. 

<ins>**`Cyclic Redundancy Check (CRC):`**</ins>

**How it works**
* Sender and Receiver on a r + 1 bit pattern knows as a generator or G for this explanation.
* For a given piece of data D, the sender will choose r additional bits R, and append them to D such that the resulting D + R is exactly divisible by G (no remainder) using modulo 2 arithmetic
* On the receiving side, the receiver divides the D + R received bits by G. If the remainder is nonzero, an error is indicated. If the remainder is zero, then no error is detected.

in CRC calculations, carries in addition and borrows from subtraction are left out. aka the arithmetic looks strikingly similar to XOR. 


### I can describe the Ethernet protocol including how it implements each of the Layer 2 services and how different versions of Ethernet differ.

`Ethernet` was the first widely deployed high-speed LAN. 

![Alt Text](/week12/Images/ethernet_frame.JPG)
* **`Data Field (46 - 1500 bytes):`** This field carries the IP datagram. If the datagram exceeds 1500 bytes, the data is fragmented. If it is under 46 bytes, the data field is "stuffed" to fill out to 46 bytes. The network layer uses the length field of IP to remove the stuffing. Additionally, although the Ethernet frame is an IP datagram, it can carry other network-layer packets as well
* **`Destination address (6 bytes):`** This field contains the MAC address of the destination adapter (ie. BB-BB-BB-BB-BB). If the receiving adapter finds that the frame is indeed intended for it, it passes the contents of the data field to the network layer. If the received frame is not intended for that adapter, it discards the frame.
* **`Source address (6 bytes):`** This field contains the MAC address of the sending adapter
* **`Type field (2 bytes):`** this field allows Ethernet to multiplex network-layer protocols. The type field helps to demultiplex the contents of the data field by indicating which network layer protocol to pass to the contents of the data field.
* **`Cyclic redundancy check (CRC)(4 bytes):`** allows receiving adapter to detect bit errors in the frame
* **`Preamble (8 bytes):`** Each of the first 7 bytes of the preamble has a value of 10101010, and the last byte is 10101011. The first 7 bytes help to "wake up" the receiving adapters and to synchronize their clocks to that of the senders. The last 2 bits of the 8th byte of the preamble alert the receiving adapter that "important" information is in route. 

Ethernet comes in many different versions standardized over the years by IEEE. 10BASE-T, 10BASE-2, 100BASE-T, 1000BASE-LX, 10GBASE-T and 40GBASE-T. The numerics in the front (10, 100, 100) refer to the speed of the standard. "BASE" refers to baseband Ethernet, noting that the physical media only carries Ethernet traffic. The final portion (most commonly "T") refers to the physical media itself ("T" = twisted-pair copper wire).
