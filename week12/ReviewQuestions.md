#### R4. Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as dprop. Will there be a collision if dprop < L/R? Why or why not?
Yes, there will be a collision if dprop < L/R. This is because the propagation delay is the time it takes for a signal to travel from one node to another, and during this time both nodes will be transmitting their packets. If the propagation delay is shorter than the time it takes to transmit a packet of length L at rate R, then the packets will collide and interfere with each other. This can result in corrupted packets, lost data, and the need for retransmission. 


#### R5. In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?
1. When only one node has data to send, that node has a throughput of R bps. `slotted ALOHA`; `token passing` 
2. When M nodes have data to send, each of these nodes have a throughput of R/M bps. `slotted ALOHA`
3. The protocol is decentralized; that is there is no master node that represents a single point of failure for the network. `slotted ALOHA`; `token passing`
4. The protocol is simple, so that it is inexpensive to implement. `slotted ALOHA`


#### R6. n CSMA/CD, after the fifth collision, what is the probability that a node chooses K = 4? The result K = 4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet? 
* The probability that a node chooses k = 4 is 1/32.
* K = 4 corresponds to a delay of 0.768 milliseconds on a 10Mbps ethernet


#### P1.  Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used. 
row 1 - 1110; parity bit = 1
row 2 - 0110; parity bit = 0
row 3 - 1001; parity bit = 0
row 4 - 0101; parity bit = 0

col 1 - 1010; parity bit = 0
col 2 - 1101; parity bit = 1
col 3 - 1100; parity bit = 0
col 4 - 0011; parity bit = 0

parity bist = 01001

#### P3. Suppose the information portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string “Internet.” Compute the Internet checksum for this data.


**Binary**: 01001001 01101110 01110100 01100101 01110010 01101110 01100101 01110100 00101110
1. Convert data to series of 16-bit integers
    * 01001001 01101110 --> 18798
    * 01110100 01100101 --> 29797
    * 01110010 01101110 --> 29294
    * 01100101 01110100 --> 25972
    * 00101110 00000000 --> 11776
2. Calculate the sum of all the 16-bit integers
    * 18798 + 29797 + 29294 + 25972 + 11776 = 115637
3. Take the one's complement of the final sum
    * toBinary(115637) = 11100001110110101
    * 1's complement == 00011110001001010


#### P6.  Consider the previous problem, but suppose that D has the value
#### a. 1000100101.
* 1000100101000000 --> 0111011010111111
#### b. 0101101010.
* 0101101010000000 --> 1010010101111111
#### c. 0110100011.
* 0110100011000000 --> 1001011100111111

#### P11.  Suppose four active nodes—nodes A, B, C and D—are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on.
#### a. What is the probability that node A succeeds for the first time in slot 4?
$$ (1-p(1-p)^3)^3*(p(1-p)^3) $$
#### b. What is the probability that some node (either A, B, C or D) succeeds in slot 5?
$$ 4p(1-p)^3$$
#### c. What is the probability that the first success occurs in slot 4?
$$ 4p(1-p)^2*(1-p)^3 $$
#### d. What is the efficiency of this four-node system? 
$$ 4p(1-p)^3 $$

#### P13.  Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is dpoll. Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel? 
$$ R*{Q \over Q+(N-1)*R*dpoll} $$ 
