#### Is your browser running HTTP version 1.0 or 1.1? What version of HTTP is the server running?
* line 1: http version 1.1, 

#### What languages (if any) does your browser indicate that it can accept to the server?
* text/html, application/xhtml+xml, application/xml 

#### What is the IP address of your computer? Of the gaia.cs.umass.edu server?
* My IP: 10.16.99.250, server IP: 128.119.245.12

#### What is the status code returned from the server to your browser?
*  Status code: 200

#### When was the HTML file that you are retrieving last modified at the server?
* Last-Modified: Tue, 05 SEP 2023 05:59:02

#### How many bytes of content are being returned to your browser?
* Content-Length: 128 bytes

#### By inspecting the raw data in the packet content window, do you see any headers within the data that are not displayed in the packet-listing window? If so, name
one.
* Server, ETag, Keep-Alive, Connection, Content-Type

#### Inspect the contents of the first HTTP GET request from your browser to the server. Do you see an “IF-MODIFIED-SINCE” line in the HTTP GET?
* No

#### Inspect the contents of the server response. Did the server explicitly return the contents of the file? How can you tell?
* No, there is no headers indicating a file was returned, ie. content-length, Accept-range, content-type, or file data.

#### Now inspect the contents of the second HTTP GET request from your browser to the server. Do you see an “IF-MODIFIED-SINCE:” line in the HTTP GET? If so, what information follows the “IF-MODIFIED-SINCE:” header?
* Last date modified 


11. What is the HTTP status code and phrase returned from the server in response to
this second HTTP GET? Did the server explicitly return the contents of the file?
Explain.
* Status code: 304 (Not Modified). No, there is no headers indicating a file was returned, ie. content-length, Accept-range, content-type, or file data.