## R5: What information is used by a process running on one host to identify a process running on another host?
In order for a process running on one host to identify a process running on another host, two pieces of information need to be specified. 
1) the address of the host, ie. (IP Address)
2) An identifier that specified the receiving process, ie.( Port number)

## R8: List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service.
1) Reliable Data Transfer (TCP)
2) Guaranteed Available Throughput 
3) Timing
4) Security (TCP)

## R11: Why do HTTP, SMTP, and IMAP run on top of TCP rather than on UDP?
Internet procols such as HTTP, SMTP, and IMAP run on top of TCP rather than UDP because TCP provides reliable data transfer. Alongside this, TCP allows for the use of TLS to provide additional security and encryption needed by these applications. UDP uses DTLS which helps with security as well but not as effective and guaranteed as TLS which cannot go over UDP. 

## R12: Consider en e-commerce site that wants to keep a purchase record for each of its customers. Describe how this can be done with cookies. 
In order to keep a record of customer history with a cookie, the following actions take place:
* Upon the first visit to a website, the server creates a unique identification number (cookie) attached to an entry in their database
* That cookie number is passed to the user and is translated back and forth whenever the user interacts with the server
* The database keeps track of all records that is performed under that particular identification number
* as long as cookies are enabled, the purchase history of the customer will always be noted in saved in the database

## R13: Describe how web caching can reduce the delay in receiving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?
A web cache is a proxy server, it serves as an entity that satisfies HTTP requests on the behalf of an origin web server. These are generally held and installed by a campus ISP, local ISP, or major ISP. When a user requests an object from a server, that object can be cached in a web cache, aka saved on the proxy server. If that user or another user on the same network wants to retrieve that object, a conditional get request can be sent instead that asks if that object has been modified on the origin server. If it has not been modified, then the proxy server can respond to the user with the object version that it has saved on its server. This process helps to reduce the delay encountered when they speed that you can reach your web cache is much faster than the origin server that the object comes from. Web caching only reduces the delay for the objects it already has and really only comes into effect when those particular objects are up to date. 

## R14: Telnet into a Web server and send a multiline request message. Include in the request message the If-modified-since: header line to force a response message with the 304 Not Modified status code.
complete


## R26: In Section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support n simultaneous connection, each from a different client host, how many sockets would the TCP server need?
When dealing with UDP and TCP socket connections, TCP needs an additional socket because one of them serves as the welcoming socket for a new connection. If the TCP server were to support n simultaneous connection, it would require (n+1) number of sockets, one for each new connection + 1 for the welcoming socket.