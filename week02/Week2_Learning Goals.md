## I can explain the HTTP message format, including the common fields.

    There are two formats a request message and a response message 
    The request message format contains 4 parts:

    1. Request line ( which contains 3 fields)
        *method field(GET, POST, HEAD, PUT, DELETE)
        * URL field
        *HTTP version field 
    2. Header lines (contains information similar to metadata about the request)
        - Some examples would be Host, Connection, User-Agent, Accept-Language
    3. Blank line (cr lf)
    4. Entity body (contains information to be sent if used as a form) 
    
    The response message format follows a very similar pattern 
    -Status line (version / status code / phrase)
    -Header lines (Connection, Date, Server, Last-Modified, Content-Length)
    -Blank line
    -Entity body (containing the requested object itself)


## I can explain the difference between HTTP GET and POST requests and why you would use each.
    GET and POST requests are both ways in which a end system can interact with a server. the GET
    request is better used when simply requesting a web page and the POST is better used when sending 
    information to the server such as a form or login credentials. The GET method can also be used 
    for sending information to the server such as a search query, but it is better for only small amounts
    of information as it shows up in the URL. The POST message is better with this because the contents
    being sent to the server are stored in the entity body, allowing it to store/send much more information. 

## I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.
    The conditional get request is in response to the issues that can arise when dealing with cached objects. 
    Because the object can be cached, the server has no access to update it on the local user side, and thats
    where the conditional get come in handy. A conditional GET request looks as follows:
    --GET /some/thing.gif HTTP/1.1
    --Host: www.plentyofthings.com
    --If-modified-since: Mon, 10 May 2023 09:38:24
    The main difference is the header that reads "if-modified-since". The cache will perform an up-to-date check 
    on the requested object and see if it has been modified since it was last cached. if it has, then it will request 
    the new version from the server. If it has not been modified, then it will send the user the cached version and 
    not waste the bandwidth that would have came with pulling the object again, particularly if it was a large object. 
    in response to an unmodified object the server will respond with a similar message to this...
    --HTTP/1.1 304 Not Modified
    --Date:
    --Server:
    (empty entity body)

## I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.
    An HTTP response code or status code as i understand it is used to indicate the result of the request. After sending 
    a request message to a server or an end system, a response message is sent back as verification as to the status of the 
    transaction. Below is the list of commonly seen status codes:
    -- 200 OK
    -- 301 Moved Permanently
    -- 400 Bad Request 
    -- 404 Not found
    -- 505 HTTP version not supported

## I can explain what cookies are used for and how they are implemented.
    in a broad level explanation, cookies are used by servers and services in order to identify a user. The first time a user 
    visits a site, they can provide a user identification, and during subsequent sessions, the browser passes a cookie header 
    to the server. As the user continues communication with the website, the cookie header is passed along their browsing history, 
    this can be used to keep track of the products that a user is looking at or the contents currently in their cart and etc. 
    while cookies bring up some privacy considerations, they also come in handy to restoring information reviewed from previous 
    sessions.

## I can explain the significance HTTP pipelining.
    in essence, traditional HTTP requires that when a client sends a request to a server, it then waits for a response prior to precessing
    subsequent requests. With pipelining, the client is able to send multiple requests along the same stream without waiting for 
    the response to be sent back. thus cutting down on the lack of resource use in waiting for a response back from the server. This is 
    typically seen when requesting a large file, or loading a video on a website that it is considerably bigger than the rest of the contents 
    on the site. 

## I can explain the significance of CDNs, and how they work.
    CDNs or Content Distribution Networks allow services to be located closer to the user sitting on the network edge. In order to meet the 
    challenge of distributing large amounts of video data to users around the world, major streaming companies make use of these CDNs. Instead of
    having the requested user fetch the content from the origin server, they can instead pull it from a nearby distribution center. these CDNs also
    have the addded benefit of being able to cache data as well as take them from other distribution networks that they are peered with. overall, 
    these CDNs greatly help with improving user-perceived delay and throughput by decreasing the number of links and routers between the end user 
    and the CDN server. They way a CDN works is as follows:
    1) the user visits a webpage on a streaming site
    2) user clicks on a video to watch
    3) the users local dns relays the query to dns for the streaming site
    4) upon seeing "video" in the string, that sites dns server then returns to the user dns a hostname in the streaming sites CDN domain
    5) the users dns sends another query to that hostname who returns an ip for a content server back to the user
    6) the users dns forwards that content server back to the user 
    7) a direct tcp connection is established and issues an HTTP GET request for the video