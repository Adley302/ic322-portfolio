### Vint Cerf is known as "one of the fathers of the Internet". Why is he known as this?
Vint Cerf is known as 'One of the fathers of the Internet' because he is a co-designer of the TCP/IP protocols and the architecture of the Internet. In December 1997, President Clinton presented the U.S. National Medal of Technology to Vint Cerf and Robert E Kahn for founding and developing the Internet. In addition, they were also received the ACM Alan Turing award in 2004 for their work on the Internet Protocols. 

### Find 3 surprising Vint Cerf facts.
1. He received the Living Legend Medal from the Library of Congress in April 2000
2. Has served as Vice President for Google
3. Worked on the Apollo program while in high school 

### Develop 2 interesting and insightful questions you'd like to ask him. For each question, describe why it is interesting and insightful.
1. As father of the internet, what are your thoughts on Artificial Intelligence
2. Did you ever imagine that your work with TCP/IP would take you this far