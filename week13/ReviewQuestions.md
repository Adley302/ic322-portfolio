Chapter 6:
(section 6.4)
#### R10. Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C’s adapter process these frames? If so, will C’s adapter pass the IP datagrams in these frames to the network layer C? How would your answers change if A sends frames with the MAC broadcast address?
> If node A sends encapsulated IP datagrams addressed to the MAC address of node B... because a broadcast lan is being used, all transmitted frames will be sent out to all connected interfaces. In this case that means that node C will receive the frames that A is trying to send to B, however, because the MAC is not addressed to C, it will drop it and not pass the frames to the network layer. 
> If node A were to change the destination MAC to the broadcast address, then both B and C will send the frame up to the network layer. 

#### R11. Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?
> An ARP query message is sent with a broadcast frame in order to be delivered to all connected devices on the subnet. The ARP response is sent within a frame of a specific MAC address to respond to the querying host. 


#### P14.  Consider three LANs interconnected by two routers, as shown in Figure 6.33.
#### a. Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 uses addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.
#### b. Assign MAC addresses to all of the adapters.
#### c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.
#### d. Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date).
![Alt Text](/week13/Images/p14.JPG)
1. Host E compiles the L3 information to include the IP address and uses its ARP table to map the target IP to its corresponding MAC address
2. The sending adapter will then construct a link-layer frame containing the destination MAC address and send the frame into the LAN

------Assuming that the ARP table in the sending host is empty...-----
1. Host E will construct an ARP query packet
2. The ARP query packet will be encapsulated in a link-layer frame using the broadcast address as its destination. 
3. The query packet will be received by all adapters on the subnet
4. The target we are looking for, if on the subnet will respond with an ARP response with destination mac address of Host E
5. Once host E receives back the information it was looking for, it will update its own ARP table and proceed to pass the original IP datagram as usual. 

#### P15. Consider Figure 6.33. Now we replace the router between subnets 1 and 2 with a switch S1, and label the router between subnets 2 and 3 as R1.
![Alt Text](/week13/Images/p15.JPG)

#### a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?
Host E will not ask router R1 to help forward the datagram because they are on the same subnet. 

Source IP/MAC: E IP/ E MAC

Destination IP/MAC: F IP/ F MAC 
#### b. Suppose E would like to send an IP datagram to B, and assume that E’s ARP cache does not contain B’s MAC address. Will E perform an ARP query to find B’s MAC address? Why? In the Ethernet frame (containing the IP datagram destined to B) that is delivered to router R1, what are the source and destination IP and MAC addresses?
Host E will send an ARP query to R1
Source IP/MAC: E IP/ E MAC
Destination IP/MAC: B IP/ R1 LAN MAC

#### c. Suppose Host A would like to send an IP datagram to Host B, and neither A’s ARP cache contains B’s MAC address nor does B’s ARP cache contain A’s MAC address. Further suppose that the switch S1’s forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A’s MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?
S1 will forward the ARP query message on both its interfaces to learn where host A resides and add host A to its forwarding table. R1 will receive the ARP query message. R1 will not forward the AR query message. Host B will not need to send an ARP query message as it already has Host As MAC.

Chapter 7:
(section 7.2)
#### R3. What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?
**Path loss:** The process by which electromagnetic radiation decreases in signal strength as it passes through matter.

**Multipath propagation:** occurs when portions of the electromagnetic wave reflect off objects and the ground, taking path of different lengths between a sender and receiver. This results in the blurring of the received signal at the receiver

**Interference from other sources:** A result of radio sources transmitting in the same frequency band as one another. This interference also occurs from electromagnetic noise within the environment ie. nearby motor, microwave. 

#### R4. As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?
1. The base station can increase its transmission power
2. Decrease transmission rate

#### P6. In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?
Unlike collision detection, 802.11 cannot abort the frame once it starts sending it. In order to avoid a costly collision, entering a random backoff is essential to preventing two stations who sense that a channel is idle at the same time from sending frames at the same time. 


#### P7. Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgment 
Time to transmit frame based off a 54Mbps:
1. Convert to bits: 1500bytes * 8 = 12,000bits
2. Calculate transmission time of 12,000 bits at a rate of 54Mpbs
3. 12,000 bits / 54,000,000 bits per second = .0002222 seconds
4. Approximately 222 microseconds
