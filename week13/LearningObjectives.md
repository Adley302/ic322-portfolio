#### I can describe the role a switch plays in a computer network.
a **switch** is a device that transfers the incoming data from more than one input ports to a specific output that will take it towards its desired destination.
To accomplish this task, a switch uses its own table that looks like this. 

![Alt Text](/week13/Images/switchTable.JPG)

Switches are considered **plug and play devices** because they require no intervention from a network administrator or user. In a computer network a switch works as follows:
1. Initially the switch table is empty
2. Each time a frame is received on an interface, the switch stores the MAC address of the of the source, the interface it came from, and the current time the new entry was added. 
3. After some time has passed known as **aging time**, the switch will purge the entry so it doesn't have to keep a hold of MACs from PCs that are no longer even connected to the network

#### I can explain the problem ARP solves, how it solves the problem, and can simulate an ARP table given a simple LAN scenario.
Because there are both network-layer addresses and link-layer addresses, there is a need to translate between them. The solution to fixing that problem is tasked to the **Address Resolution Protocol (ARP)**.

Each host and router has an ARP table in its memory that looks similar to this:
![ALT TEXT](/week13/Images/arpTable.JPG)

An ARP table consists of an IP Address -> MAC Address mapping as well as a time-to-live (TTL) field. the TTL indicates when each mapping will be deleted from the table. a typical expiration time for an entry is 20 minutes from when an entry is placed in an ARP table. 

If an entry is not in the ARP table, the following steps are taken:
1. The sender constructs a special packet called an **ARP packet**. The purpose of the ARP query packet is to query all the other hosts and routers on the subnet to determine the MAC address corresponding to the IP address that is being resolved. 
2. The ARP query packet is passed to the adapter with an indication that the adpater should send the packet to the MAC broadcast address (FF-FF-FF..)
3. The frame containing the ARP query is received by all other adapters on the subnet. 
4. Each adapter passes the ARP packet packet within the frame up to its ARP module. Each of these ARP modules then check to see if its IP address matches the destination IP address in the ARP packet. 
5. The one with a match then sends back to the querying host a response ARP packet with the desired mapping. 
6. After getting back to the querying host, that host can then update its ARP table.  


#### I can explain CSMA/CA, how it differs from CSMA/CD, what problems it addresses, and how it solves them. 
**Carrier Sense Multiple Access with Collision Avoidance (CSMA/CA)** is a MAC protocol used in wireless networks to manage the access to the communication channel and avoid collision. 
* Each station senses the channel before transmitting
* Each station refrains from transmitting when the channel is sensed busy

CSMA/CA does not use collision detection schemes like its ethernet counterpart because the strength of a received signal is typically very small compared to the strength of the transmitted signal at the 802.11 adapter, it is costly to build hardware that can detect a collision. Additionally, even if the adapter could transmit and listen at the same time, it wouldnt be able to detect all collision due to the hidden terminal problem and fading.

The Steps of the CSMA/CA Protocol are as follows:
1. If initially the station senses the channel idle, it transmits its frame after a 
short period of time known as the Distributed Inter-frame Space (DIFS)
2. Otherwise, the station chooses a random backoff value using binary exponential backoff and counts down this value after DIFS when the channel is sensed idle. While the channel is sensed busy, the counter value remains frozen.
3. When the counter reaches zero (note that this can only occur while the channel is sensed idle), the station transmits the entire frame and then waits for an acknowledgment.
4. If an acknowledgment is received, the transmitting station knows that its frame 
has been correctly received at the destination station. If the station has another 
frame to send, it begins the CSMA/CA protocol at step 2. If the acknowledgment isn’t received, the transmitting station reenters the backoff phase in step 2, 
with the random value chosen from a larger interval.
