*Adley Louissaint  | IC322 Portfolio* 

Welcome to my assignments portfolio for IC322: Computer Networks

* [Week 1: The Internet](/week01/)
* [Week 2: Application Layer: HTTP](/week02/)
* [Week 3: Application Layer: DNS](/week03/)
* [Week 4: Transport Layer](/week04/)
* [Week 5: Transport Layer pt2](/week05/)
* [Week 6: Six Week Assessment](/week06/)
* [Week 7: Network Layer - Data Plane](/week07/)

