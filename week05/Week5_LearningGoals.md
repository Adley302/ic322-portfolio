## I can explain the problem that TCP congestion control is solving for, and how it solves that problem.
TCP congestion control attempts to solves the problem of any one TCP connection swamping the links and 
routers between communicating hosts with an excess amount of traffic. To solve this problem, TCP has each 
sender limit the rate at which it sends traffic into its connection as a function of perceived network congestion. 
if there is little perceived congestion, the sender increases its send rate, if there is perceived congestion, then
the sender decreases its send rate. In order to this, TCP utilizes slow start, congestion avoidance, and fast recovery. 

## I can explain the problem that TCP flow control is solving for, and how it solves that problem.
TCP flow control aims to solve the issues of having the sender overflow the receivers buffer. In order to do this
TCP flow control has the sender maintain a variable called the receive window that is used to give the sender an
idea of how much free buffer space is available at the receiver. 
The receiver window follows this formula:
#### RecvWindow = RecvBuffer - (LastByteRcvd - LastByteRead)