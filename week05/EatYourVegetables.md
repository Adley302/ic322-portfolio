# R17.  Suppose two TCP connections are present over some bottleneck link of rate Rbps. Both connections have a huge file to send (in the same direction over the bottleneck link). The transmissions of the files start at the same time. What transmission rate would TCP like to give to each of the connections?
TCP will try to split the transmittion rates evenly by assigning each of the connections a rate of R/2.

# R18. True or false? Consider congestion control in TCP. When the timer expires at the sender, the value of ssthresh is set to one half of its previous value.
true

# P27.  Host A and B are communicating over a TCP connection, and Host B has already received from A all bytes up through byte 126. Suppose Host A then sends two segments to Host B back-to-back. The first and second segments contain 80 and 40 bytes of data, respectively. In the first segment, the sequence number is 127, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgment whenever it receives a segment from Host A.
## a. In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?
* The sequence number will 126+1 = **127**
* The source port number will be remains the same **302** 
* The destination port number remains the same **80**
## b. If the first segment arrives before the second segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number, the source port number, and the destination port number?
* The acknowledgement number will be 127+80 = **206**
* The source port number will be remains the same **302** 
* The destination port number remains the same **80**
## c. If the second segment arrives before the first segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number?
* the acknowledgment number will be **127** 
## d. Suppose the two segments sent by A arrive in order at B. The first acknowledgment is lost and the second acknowledgment arrives after the first timeout interval. Draw a timing diagram, showing these segments and all other segments and acknowledgments sent. (Assume there is no additional packet loss.) For each segment in your figure, provide the sequence number and the number of bytes of data; for each acknowledgment that you add, provide the acknowledgment number.
1) --------------> seq = 127, 80 bytes of data
2) 
2) X(loss) <-----
3) --------------> seq = 127

# P33. In Section 3.5.3, we discussed TCP’s estimation of RTT. Why do you think TCP avoids measuring the SampleRTT for retransmitted segments?
Measuring RTT of retransmitted segment is a not a good estimate of the actual RTT. Network conditions may have changed since the original segment. In addition, a retransmitted segment means something went wrong...aka taking that as the measurements of normal RTT could result in an inaccurate reaction such as needlessly increasing the congestion window.


# P36. In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?
TCP desginers chose to wait until three duplicates have been detected to prevent unnecessary retransmissions. There are multiple reasons why a single or even a double duplicate ack has been received, by waiting for three duplicates, we can ensure that we truly need to send back the transmissions and that they're not just lost in traffic and still making there way over the network. 

# P40. Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer.
## a. Identify the intervals of time when TCP slow start is operating.
* the congestion window raises exponentially between rounds 1 - 6. illustrating the slow start window
## b. Identify the intervals of time when TCP congestion avoidance is operating.
* congestion window is evident in a linear increase in the graph, this is observed between rounds 7-16
## c. After the 16th transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?
* a segment loss due to a triple duplicate drops the congestion window to a number above 1, however a segment loss due to a timeout drops the congestion window size to 1. Therefore, as illustrated by the graph, after the 16th transmission round the segment loss is a **Triple duplicate**
## d. After the 22nd transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?
* As explained above, the segment loss after the 22nd round must therefore be a **timeout**