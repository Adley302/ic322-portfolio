#### R20. In the TLS record, there is a field for TLS sequence numbers. True or false?
* False 

#### R21. What is the purpose of the random nonces in the TLS handshake? 
nonces are used in the creation of session keys. They are used to defend against connection replay attacks by generating different encryption keys for each TCP session. 

#### R22. Suppose a TLS session employs a block cipher with CBC. True or false: The server sends to the client the IV in the clear.
* True

#### R23. Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice’s certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?
At step 3 of the TLS handshake Bob will encrypt the Pre-Master Secret (PMS) with Alice's public key and send it to Trudy (the imposter). At this point, Trudy won't be able to compute the Master Secret because he does not have Alice's private key to decrypt the PMS. When Trudy goes to send Bob the HMAC of all the handshake messages, it will be clear that it wasn't really Alice on the other side. 

#### P9. In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key. The DH algorithm makes use of a large prime number p and another large number g less than p. Both p and g are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys, SA and SB, respectively. Alice then computes her public key, TA, by raising g to SA and then taking mod p. Bob similarly computes his own public key TB by raising g to SB and then taking mod p. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key S by raising TB to SA and then taking mod p. Similarly, Bob calculates the shared key S′ by raising TA to SB and then taking mod p.

#### a. Prove that, in general, Alice and Bob obtain the same symmetric key, that is, prove S = S′.
Since g is a root modulo p,  g^SASB = g^SASB mod p; S = S’

#### b. With p = 11 and g = 2, suppose Alice and Bob choose private keys SA = 5 and SB = 12, respectively. Calculate Alice’s and Bob’s public keys, TA and TB. Show all work.
Alices public key: 2^5(mod 11) == 1
Bobs public key: 2^12(mod 11) == 9

#### c. Following up on part (b), now calculate S as the shared symmetric key. Show all work.
Shared symmetric key: raising TB to the power of SA  9^5(mod 11) == 4

#### d. Provide a timing diagram that shows how Diffie-Hellman can be attacked by a man-in-the-middle. The timing diagram should have three vertical lines, one for Alice, one for Bob, and one for the attacker Trudy.
| Time | Alice | Bob | Trudy |
|------|-------|-----|-------|
| t1   |chooses private key SA |chooses privatekey SB | |
| t2   |computes TA = 9^SB(mod p) |computes TB = 9^SB(mod p) | |
| t3   |sends TA to Bob | sends TB to Alice| |
| t4   | |receives TA | intercepts TA |
| t5   |receives TB | | intercepts TB |
| t6   |computes S = TB^SA(mod p) | computes S' = TA^SB(mod p) | computes S'' = TB^ST(mod p) |
| t7   |  |  | decrypts message using S'' |

#### P14. The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?
Communication entities are only concerned about message integrity rather than message confidentiality. Using a Message Authentication Code (MAC), the entities can authenticate the messages without having to integrate complex encryption algorithms in the to integrity process. 

#### P23. Consider the example in Figure 8.28. Suppose Trudy is a woman-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.
R2 will be able to detect the duplicate datagram because of the sequence numbers used in IPSec.
