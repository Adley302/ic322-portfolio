### What is WannaCry

WannaCry was a ransomware attack targetting windows computers that spread across the globe in May of 2017. WannaCry encrypted a users computers, denying them access and requesting a ransom of $300 in bitcoin to stop the worm from wiping all contents off the computer. The attack was estimated to have affected over 230,000 computers across 150 countries. While traditional ransomware attacks of the past have had the distinct objective of going after a particular target, WannaCry ransomware went after multiple targets across the globe. Due to the large number of vulnerable unupdated windows computers, WannaCry was able to spread to thousands of computers in just a few short hours. 

### How does it work?
WannaCry is a worm type of ransomware. A worm is a type of malware that can spread very fast across systems because it can self-propagate without the nedd for human interaction or activation. 


### Where did it come from?


### How did it get taken down?


### Is it still active today?


### What sectors were hit the hardest by WannaCry


### What are the ethical complications of WannaCry?


### What are the legal considerations of WannaCry?


### What are the professional responsibilities of computing professionals for attacks such as these?


### Closing statement

## Sources:
* https://www.malwarebytes.com/wannacry 
* https://www.csoonline.com/article/563017/wannacry-explained-a-perfect-ransomware-storm.html