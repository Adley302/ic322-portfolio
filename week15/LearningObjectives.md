#### I can explain how two strangers are able to exchange secret keys in a public medium.

#### I can walk through the TLS handshake and explain why each step is necessary.
1. The client sends a list of cryptographic algorithms it supports, along with a 
client nonce.
 * the list helps to decide which algorithms will be supported between the two hosts, and a nonce is used to combat replay attacks by changing the session keys in each session. 
2. From the list, the server chooses a symmetric algorithm (for example, AES) 
and a public key algorithm (for example, RSA with a specific key length), and 
HMAC algorithm (MD5 or SHA-1) along with the HMAC keys. It sends back 
to the client its choices, as well as a certificate and a server nonce.
 * This step once again confirms the agreed upon algorithm and a nonce in order to combat a replay attack of the message
3. The client verifies the certificate, extracts the server’s public key, generates a 
Pre-Master Secret (PMS), encrypts the PMS with the server’s public key, and 
sends the encrypted PMS to the server.
 * 
4. Using the same key derivation function (as specified by the TLS standard), 
the client and server independently compute the Master Secret (MS) from the 
PMS and nonces. The MS is then sliced up to generate the two encryption and 
two HMAC keys. Furthermore, when the chosen symmetric cipher employs 
CBC (such as 3DES or AES), then two Initialization Vectors (IVs)—one for 
each side of the connection—are also obtained from the MS. Henceforth, all 
messages sent between client and server are encrypted and authenticated (with 
the HMAC).
 * Step taken to ensure message integrity 
5. The client sends the HMAC of all the handshake messages.
6. The server sends the HMAC of all the handshake messages.
* steps 5 & 6 are used as receipts that no tampering occured during the handshake process by a third party


#### I can explain how TLS preventsv man-in-the-middle attacks.
Transport Layer Security (TLS) was designed to help provide a secure communication and thus comes with several mechanisms to prevent man-in-the-middle attacks. 
* Encryption 
* Authnetication
* Handshake process itself
* Cert validation
* Nonces
Starting with the initial handshake, TLS ensures that data is thoroughly encrypted, authenticated, unique session IDs, message integrity, and certificate authority is valid.